package com.mapper;

import com.bean.Choice;
import com.bean.ExamPaper;
import com.bean.Judge;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ExamPaperMapper {
    ExamPaper queryExamPaperById(@Param("id")String id);

    List<Judge> queryJudgeListByExamId(@Param("examid")String examid);

    List<Choice> queryChoiceListByExamId(@Param("examid")String examid);

    int addExamPaperChoice(@Param("examid")String examid,@Param("choiceid")String choiceid);

    int addExamPaperJudge(@Param("examid")String examid,@Param("judgeid")String judgeid);
}
