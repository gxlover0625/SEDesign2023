package com.mapper;

import com.bean.ExamInfomation;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

import java.util.List;

public interface ExamMapper {
    List<ExamInfomation> queryExamByTid(@Param("tid")String tid);

    int addExam(ExamInfomation examInfomation);

    int queryExamId();

    ExamInfomation queryExamInfoByExamId(@Param("examid")String examid);
}
