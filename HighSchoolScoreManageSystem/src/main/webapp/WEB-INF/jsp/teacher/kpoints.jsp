<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html;charset=utf-8" isELIgnored="false" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>高校成绩管理系统</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="<%= basePath%>resources/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

      <script src="<%=basePath%>resources/vendor/js/123.js"></script>
      <script src="<%=basePath%>resources/vendor/js/456.js"></script>

      <style class="cp-pen-styles">* {
          margin: 0;
          padding: 0;
          -webkit-box-sizing: border-box;
          -moz-box-sizing: border-box;
          box-sizing: border-box;
      }

      ul {
          list-style-type: none;
      }
      /*h1 {
          color: #FFF;
          font-size: 24px;
          font-weight: 400;
          text-align: center;
          margin-top: 80px;
      }*/

      h1 a {
          color: #c12c42;
          font-size: 16px;
      }

      .accordion {
          width: 100%;
          max-width: 1000px;
          margin: 30px auto 20px;
          background: #FFF;
          -webkit-border-radius: 4px;
          -moz-border-radius: 4px;
          border-radius: 4px;
      }

      .accordion .link {
          cursor: pointer;
          display: block;
          padding: 15px 15px 15px 42px;
          color: #4D4D4D;
          font-size: 14px;
          font-weight: 700;
          border-bottom: 1px solid #CCC;
          position: relative;
          -webkit-transition: all 0.4s ease;
          -o-transition: all 0.4s ease;
          transition: all 0.4s ease;
      }

      .accordion li:last-child .link {
          border-bottom: 0;
      }

      .accordion li i {
          position: absolute;
          top: 16px;
          left: 12px;
          font-size: 18px;
          color: #595959;
          -webkit-transition: all 0.4s ease;
          -o-transition: all 0.4s ease;
          transition: all 0.4s ease;
      }

      .accordion li i.fa-chevron-down {
          right: 12px;
          left: auto;
          font-size: 16px;
      }

      .accordion li.open .link {
          color: #55acee;
      }

      .accordion li.open i {
          color: #55acee;
      }
      .accordion li.open i.fa-chevron-down {
          -webkit-transform: rotate(180deg);
          -ms-transform: rotate(180deg);
          -o-transform: rotate(180deg);
          transform: rotate(180deg);
      }

      .accordion li.default .submenu {display: block;}
      /**
       * Submenu
       -----------------------------*/
      .submenu {
          display: none;
          font-size: 14px;
      }

      .submenu li {
          border-bottom: 1px solid #4b4a5e;
      }

      .submenu a {
          display: block;
          text-decoration: none;
          color: #55acee;
          padding: 12px;
          padding-left: 42px;
          -webkit-transition: all 0.25s ease;
          -o-transition: all 0.25s ease;
          transition: all 0.25s ease;
      }

      .submenu a:hover {
          background: #b63b4d;
          color: #FFF;
      }

      </style>
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="<%=path%>/admin/info">教务管理系统</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="<%=basePath%>logout"><i class="fa fa-sign-out fa-lg"></i> 退出</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
        <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" style="width: 80px; height: 80px;" src="<%=basePath%>resources/img/user.png" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">${teacher.name}</p>
          <p class="app-sidebar__user-designation">教师</p>
        </div>
      </div>
      <ul class="app-menu">
          <li><a class="app-menu__item " href="<%=path%>/teacher/index"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">个人信息</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/courses"><i class="app-menu__icon fa fa fa-book"></i><span class="app-menu__label">我的任课</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/myStudent"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">我的学生</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/score"><i class="app-menu__icon fa fa-align-left"></i><span class="app-menu__label">学生成绩</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/scoreInsert"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">成绩录入</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/check"><i class="app-menu__icon fa fa-check"></i><span class="app-menu__label">发起签到</span></a></li>
          <li><a class="app-menu__item check.jsp" href="<%=path%>/teacher/kpoints"><i class="app-menu__icon fa fa-bookmark-o"></i><span class="app-menu__label">知识点</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/tk"><i class="app-menu__icon fa fa-list-alt"></i><span class="app-menu__label">题库-判断题</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/tkchoice"><i class="app-menu__icon fa fa-list-alt"></i><span class="app-menu__label">题库-选择题</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/examinfo"><i class="app-menu__icon fa fa-history"></i><span class="app-menu__label">考试历史</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/updatePwd"><i class="app-menu__icon fa fa-certificate"></i><span class="app-menu__label">修改密码</span></a></li>
      </ul>
    </aside>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-bookmark-o"></i> 知识点</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="<%=path%>/teacher/kpoints">知识点</a></li>
        </ul>
      </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <!-- Page Content -->
<%--                    <div id="page-wrapper">--%>
                        <h1 align="center">知识点汇总</h1>
                        <!-- Contenedor -->

                        <ul id="accordion" class="accordion">
                            <c:forEach items="${point1}" var="ts">
                                <li>
                                    <div class="link"><i class="fa fa-paint-brush">
                                        </i>${ts.pointname}<i class="fa fa-chevron-down"></i>
                                    </div>
                                    <ul class="submenu">
                                        <c:forEach items="${ts.point2}" var="t">
                                            <li><a href="#">${t.pname}</a></li>
                                        </c:forEach>
                                    </ul>
                                </li>
                            </c:forEach>
                        </ul>
                        </ul>
                        <button  class="btn btn-info" id="addpoint1">添加章节</button>
                        <button  class="btn btn-info" id="addpoint2">添加小节</button>
                        <button  class="btn btn-info" id="delpoint1">删除章节</button>
                        <button  class="btn btn-info" id="delpoint2">删除小节</button>
<%--                    </div>--%>
                </div>
            </div>
        </div>
    </main>

    <!-- /#wrapper --><!-- 添加章节的模态框 -->
    <div class="modal fade" id="addp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<%--                    <h4 class="modal-title" id="myModalLabel">章节添加</h4>--%>
                </div>
                <form class="form-horizontal" method="post" action="<%=basePath%>/teacher/addpoint1">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">章节名</label>
                            <div class="col-sm-10">
                                <input type="text" name="pointname" class="form-control" id="pointname" placeholder="pointname">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary" id="empbtn">保存</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- /#wrapper --><!-- 删除章节的模态框 -->
    <div class="modal fade" id="delp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<%--                    <h4 class="modal-title" id="mypoint1">删除章节</h4>--%>
                </div>
                <form class="form-horizontal" action="<%=basePath%>/teacher/delepoint1" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">章节</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="delp1_select" name = "id"></select>
                                <!--<option value="1"></option>-->
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary" id = "delp1but">删除</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div>
    </div>


    <!-- 添加小节的模态框 -->
    <div class="modal fade" id="addp2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<%--                    <h4 class="modal-title" id="myModalLabel">章节添加</h4>--%>
<%--                    <h4 class="modal-title" id="my">添加小节</h4>--%>
                </div>
                <form class="form-horizontal" action="<%=basePath%>/teacher/addpoint2" id="addpoint2form" name=addpoint2form" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">章节</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="addp2_select" name = "aid"></select>
                                <!--<option value="1"></option>-->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">小节</label>
                            <div class="col-sm-10">
                                <input type="text" name="pname" class="form-control" id="pname" placeholder="">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        <button type="button" class="btn btn-primary" id = "addp2but">提交</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- 删除小节的模态框 -->
    <div class="modal fade" id="delp2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<%--                    <h4 class="modal-title" id="myp2">删除小节</h4>--%>
                </div>
                <form class="form-horizontal" action="<%=basePath%>/teacher/delepoint2" method="post" id="delepoint2form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">章节</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="chapter" name = "chapter">
                                </select>
                                <select class="form-control" id="chaptertwo" name = "chaptertwo"></select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary" id = "delp2but">提交</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <!-- Essential javascripts for application to work-->
    <script src="<%= basePath%>resources/js/jquery-3.2.1.min.js"></script>
    <script src="<%= basePath%>resources/js/popper.min.js"></script>
    <script src="<%= basePath%>resources/js/bootstrap.min.js"></script>
    <script src="<%= basePath%>resources/js/main.js"></script>
    <script src="<%= basePath%>resources/vendor/metisMenu/metisMenu.min.js"></script>
    <script src="<%= basePath%>resources/vendor/dist/js/sb-admin-2.js"></script>


    <!-- Data table plugin-->
    <script type="text/javascript" src="<%= basePath%>resources/js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<%= basePath%>resources/js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">$('#scoreTable').DataTable();</script>

    <script>
        $(function () {
            var Accordion = function (el, multiple) {
                this.el = el || {};
                this.multiple = multiple || false;
                var links = this.el.find('.link');
                links.on('click', {
                    el: this.el,
                    multiple: this.multiple
                }, this.dropdown);
            };
            Accordion.prototype.dropdown = function (e) {
                var $el = e.data.el;
                $this = $(this), $next = $this.next();
                $next.slideToggle();
                $this.parent().toggleClass('open');
                if (!e.data.multiple) {
                    $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
                }
                ;
            };
            var accordion = new Accordion($('#accordion'), false);
        });
        //@ sourceURL=pen.js
    </script>

  </body>

  <script>
      //添加章节
      $("#addpoint1").click(function(){
          $("#addp1").modal({
              backdrop:"static"
          });
      });
      //添加小节
      $("#addpoint2").click(function(){
          $("#addp2_select option").remove();
          getpoint1();
          $("#addp2").modal({
              backdrop:"static"
          });
      });
      //删除章节
      $("#delpoint1").click(function(){
          //清除表单数据（表单完整重置（表单的数据，表单的样式））
          // reset_form("#addp1 form");
          //发送ajax请求，查出，显示在下拉列表中
          //弹出模态框
          $("#delp1_select option").remove();
          getpoint1();
          $("#delp1").modal({
              backdrop:"static"
          });
      });
      //删除小节
      $("#delpoint2").click(function(){
          $("#chapter option").remove();
          $("#chaptertwo option").remove();
          getpoint();
          $("#delp2").modal({
              backdrop:"static"
          });
      });
      function getpoint() {
          $.ajax({
              url:"${APP_PATH}/teacher/allpoint1",
              type:"GET",
              success:function(data) {
                  console.log(data);
                  var chapter =$(document).find("#chapter");
                  var chaptertwo =$(document).find("#chaptertwo");
                  var  num= data.length;
                  //   console.log("长度是"+num);
                  var pp = new Array(num+1);
                  for (var i=0;i<data.length;i++){
                      chapter.append("<option value='" + i + "'>" + data[i].pointname + "</option>");
                  }
                  for(var j=0;j<data[0].point2.length;j++){
                      chaptertwo.append("<option value='" + data[0].point2[j].bid + "'>" + data[0].point2[j].pname + "</option>");
                  }
                  //select1绑定change事件
                  $("#chapter").change(function () {
                      var p = this.value;
                      console.log("第几个"+p);
                      $("#chaptertwo").empty();
                      //遍历p2的数组
                      console.log("长度为"+ data[p].point2.length);
                      for (var j = 0; j < data[p].point2.length; j++) {
                          console.log(data[p].point2[j].bid,data[p].point2[j].pname);
                          chaptertwo.append("<option value='" + data[p].point2[j].bid+ "'>" + data[p].point2[j].pname+ "</option>");
                      }
                  });
              }
          });
      }
      function getpoint1() {
          $.ajax({
              url:"${APP_PATH}/teacher/allpoint1",
              type:"GET",
              success:function(data) {
                  //console.log(data);
                  var bb =$(document).find("#delp1_select");
                  var aa =$(document).find("#addp2_select");
                  for (var i = 0; i < data.length; i++) {
                      aa.append("<option value='" + data[i].id+ "'>" + data[i].pointname + "</option>");
                      bb.append("<option value='" + data[i].id+ "'>" + data[i].pointname + "</option>");
                  }
              }
          });
      }
      $("#addp2but").click(function(){
          // alert($("#addp2 form").serialize());//序列化form上的数据
          <%--$.ajax({--%>
          <%--    url:"${APP_PATH}/teacher/addpoint2",--%>
          <%--    type:"POST",--%>
          <%--    data:$("#addp2 form").serialize(),--%>
          <%--    success:function() {--%>

          <%--        $("#addp2").modal('hide');--%>
          <%--    }--%>
          <%--});--%>
          $('#addpoint2form').submit();
          /*alert(2)
          $('#addpoint2form').submit(function () {
                alert(1)
          })*/
      });
      //清空表单样式及内容
      function reset_form(ele){
          $(ele)[0].reset();
          //清空表单样式
          $(ele).find("*").removeClass("has-error has-success");
          $(ele).find(".help-block").text("");
      }
  </script>
</html>