package com.bean;

/**
 * Description: 专业实体类
 */
public class Department {
    String ID;
    String name;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
