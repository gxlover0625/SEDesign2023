package com.service.impl;

import com.bean.Check;
import com.bean.CheckRecord;
import com.mapper.CheckMapper;
import com.mapper.ClassMapper;
import com.service.CheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CheckServiceImpl implements CheckService {
    @Autowired
    CheckMapper checkMapper;
    @Override
    public List<Check> getCheckList(String id) {
        return checkMapper.selectCheck(id);
    }

    @Override
    public String getCidbyBeginTime(long beginTime) {
        return checkMapper.selectCidByBeginTime(beginTime);
    }

    @Override
    public void insertRecord(String cid, String Sno) {
        checkMapper.insertRecord(cid, Sno);
    }

    @Override
    public List<CheckRecord> getRecordList(String cid) {
        return checkMapper.getRecordList(cid);
    }

    @Override
    public List<CheckRecord> getRecordListBySno(String Sno) {
        return checkMapper.selectRecordListBySno(Sno);
    }

    @Override
    public void updateRecord(String id, String cid, String cur) {
        checkMapper.updateRecord(id, cid, cur);
    }

    @Override
    public Check getCheckByCid(String cid) {
        return checkMapper.selectCheckByCid(cid);
    }
}
