package com.service.impl;

import com.bean.ExamInfomation;
import com.mapper.ExamMapper;
import com.service.ExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExamServiceImpl implements ExamService {
    @Autowired
    ExamMapper examMapper;

    @Override
    public List<ExamInfomation> queryExamByTid(String tid) {
        return examMapper.queryExamByTid(tid);
    }

    @Override
    public int addExam(ExamInfomation examInfomation) {
        return examMapper.addExam(examInfomation);
    }

    @Override
    public int queryExamId() {
        return examMapper.queryExamId();
    }

    @Override
    public ExamInfomation queryExamInfoByExamId(String examid) {
        return examMapper.queryExamInfoByExamId(examid);
    }
}
