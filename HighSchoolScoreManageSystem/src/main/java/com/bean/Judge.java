package com.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Judge {
    private Long id;
    private String content;
    private String answer;
    private String analysis;
    private String chapterId;
    private String chapter;
    private String chaptertwo;
    private String difficulty;
}
