package com.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

import java.util.List;

public interface ClassMapper {
    List<Class> selectClass();

    void insertClass(@Param("departmentID") String departmentID,@Param("classID") String classID);

    void deleteClass(@Param("id") String id);

    void updateClassDno(@Param("departmentId") String departmentId, @Param("oldId") String oldId);
    void updateClassno(@Param("id") String id, @Param("oldId") String oldId);
}
