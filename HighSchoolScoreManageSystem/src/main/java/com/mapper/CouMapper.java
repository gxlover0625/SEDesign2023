package com.mapper;

import com.bean.Course;
import com.bean.StuCourse;
import com.bean.TeaCourse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CouMapper {
    List<Course> selectCouList();

    void insertCou(Course course);

    List<Course> selectClassCouList();

    void insertClassCou(@Param("classNo") String classNo,@Param("ID") String id);

    List<Course> selectStuCouList(@Param("id") String id);

    List<Course> selectCouByClass(@Param("classNo") String classNo);

    void insertSC(@Param("ID") String id,@Param("courseId") String courseId);

    void deleteSC(@Param("ID") String id,@Param("courseId") String courseId);

    List<TeaCourse> selectTeaCouList(@Param("id") String id);

    void deteleCou(@Param("id") String id);

    Course selectCouById(@Param("id") String id);

    void updateCou(@Param("course") Course course);

    void deleteClassCou(@Param("classNo") String classNo,@Param("id") String id);

    List<String> selectClassidByTid(@Param("tid")String tid);
}
