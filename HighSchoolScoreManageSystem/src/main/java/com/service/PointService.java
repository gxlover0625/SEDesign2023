package com.service;

import com.bean.Point1;
import com.bean.Point2;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

public interface PointService {
    List<Point1> findAllPoint1();

    List<Point1> findAllPoint1ByTeaId(String tid);

    int insertPoint1(String cname);

    int insertPointOwner(String cid, String tid);

    int getLastId();

    int insertPoint2(String aid,String pname);

    int deletePoint1(String kid,String tid);

    int deletePoint2(String bid);

    List<Point2> queryPoint2ByPoint1Id(String cid);
}
