package com.mapper;

import com.bean.Judge;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface JudgeMapper {
    List<Judge> queryAllJudge();

    List<Judge> queryAllJudgeByTid(@Param("tid")String tid);

    List<Judge> queryJudgeByCid(@Param("cid")String cid);

    List<Judge> queryJudgeByExamId(@Param("examid") String exmaid);

    int insertJudge(Map<String,String> map);

    int deleteJudgeById(@Param("jid")String jid);

    Judge queryJudgeById(@Param("jid")String jid,@Param("tid")String tid);
}
