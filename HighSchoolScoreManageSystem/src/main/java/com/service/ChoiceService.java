package com.service;

import com.bean.Choice;
import com.bean.Judge;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChoiceService {
    List<Choice> queryAllChoiceByTid(String tid);

    List<Choice> queryChoiceByCid(String cid);

    int insertChoice(Choice choice);

    int deleteChoice(String chid);

    List<Choice> queryChoiceByExamId(String examid);
}
