<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html;charset=utf-8" isELIgnored="false" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>高校成绩管理系统</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="<%= basePath%>resources/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="<%=path%>/student/index">教务管理系统</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="<%=basePath%>logout"><i class="fa fa-sign-out fa-lg"></i> 退出</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
        <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" style="width: 80px; height: 80px;" src="<%=basePath%>resources/img/user.png" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">${student.name}</p>
          <p class="app-sidebar__user-designation">学生</p>
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item" href="<%=path%>/student/index"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">个人信息</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/student/courses"><i class="app-menu__icon fa fa fa-book"></i><span class="app-menu__label">我的课程</span></a></li>
        <li><a class="app-menu__item active" href="<%=path%>/student/score"><i class="app-menu__icon fa fa-align-left"></i><span class="app-menu__label">成绩查询</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/student/classCourse"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">选课</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/student/check"><i class="app-menu__icon fa fa-bell"></i><span class="app-menu__label">课程签到</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/student/article"><i class="app-menu__icon fa fa-comments"></i><span class="app-menu__label">学生论坛</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/student/myArticle"><i class="app-menu__icon fa fa-comment"></i><span class="app-menu__label">我的发言</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/student/updatePwd?reqForward=true"><i class="app-menu__icon fa fa-certificate"></i><span class="app-menu__label">修改密码</span></a></li>

      </ul>
    </aside>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-align-left"></i> 成绩查询</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="<%=path%>/student/score">成绩查询</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <table class="table table-hover table-bordered" id="scoreTable">
                <thead>
                  <tr>
                    <th>学期</th>
                    <th>课程名称</th>
                    <th>任课教师</th>
                    <th>学时</th>
                    <th>学分</th>
                    <th>考查类型</th>
                    <th>成绩</th>
                  </tr>
                </thead>
                <tbody>
                    <c:forEach items="${stuScoreList}" var="score" varStatus="">
                        <tr>
                            <td>${score.term}</td>
                            <td>${score.name}</td>
                            <td>${score.teacher}</td>
                            <td>${score.hours}</td>
                            <td>${score.credit}</td>
                            <td>${score.type}</td>
                            <td class="score">${score.score}</td>
                        </tr>
                    </c:forEach>
                  </tr>
                </tbody>
              </table>
              <div class="offset-md-11" style="margin-top: 18px" id="avg">平均成绩：${avgScore}</div>
              <script>
                cal()
                document.onkeyup = cal
                function cal(e){
                  var avg = document.getElementById('avg')
                  var score = document.getElementsByClassName('score')

                  var sum = 0;
                  for(var i = 0; i < score.length; i++){
                    sum += parseInt(score[i].innerHTML)
                  }
                  var res = 0;
                  if(score.length > 0){
                    res = sum / score.length
                    res = res.toFixed(2)
                  }
                  else{
                    res = 0
                  }
                  avg.innerHTML = '平均成绩：' + res
                }
              </script>
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="<%= basePath%>resources/js/jquery-3.2.1.min.js"></script>
    <script src="<%= basePath%>resources/js/popper.min.js"></script>
    <script src="<%= basePath%>resources/js/bootstrap.min.js"></script>
    <script src="<%= basePath%>resources/js/main.js"></script>
    <!-- Data table plugin-->
    <script type="text/javascript" src="<%= basePath%>resources/js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<%= basePath%>resources/js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">$('#scoreTable').DataTable();</script>
  </body>
</html>