<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html;charset=utf-8" isELIgnored="false" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>高校成绩管理系统</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="<%= basePath%>resources/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="<%=path%>/teacher/index">教务管理系统</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="<%=basePath%>logout"><i class="fa fa-sign-out fa-lg"></i> 退出</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
        <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" style="width: 80px; height: 80px;" src="<%=basePath%>resources/img/user.png" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">${sessionScope.teacher.name}</p>
          <p class="app-sidebar__user-designation">教师</p>
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item" href="<%=path%>/teacher/index"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">个人信息</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/courses"><i class="app-menu__icon fa fa-book"></i><span class="app-menu__label">我的任课</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/myStudent"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">我的学生</span></a></li>
        <li><a class="app-menu__item active" href="<%=path%>/teacher/score"><i class="app-menu__icon fa fa-align-left"></i><span class="app-menu__label">学生成绩</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/scoreInsert"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">成绩录入</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/check"><i class="app-menu__icon fa fa-check"></i><span class="app-menu__label">发起签到</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/kpoints"><i class="app-menu__icon fa fa-bookmark-o"></i><span class="app-menu__label">知识点</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/tk"><i class="app-menu__icon fa fa-list-alt"></i><span class="app-menu__label">题库-判断题</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/tkchoice"><i class="app-menu__icon fa fa-list-alt"></i><span class="app-menu__label">题库-选择题</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/examinfo"><i class="app-menu__icon fa fa-history"></i><span class="app-menu__label">考试历史</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/updatePwd"><i class="app-menu__icon fa fa-certificate"></i><span class="app-menu__label">修改密码</span></a></li>

      </ul>
    </aside>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-align-left"></i> 学生成绩</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="<%=path%>/teacher/score">学生成绩</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <table class="table table-hover table-bordered" id="scoreTable">
                <thead>
                  <tr>
                    <th>序号</th>
                    <th>学期</th>
                    <th>班级</th>
                    <th>学号</th>
                    <th>学生姓名</th>
                    <th>课程</th>
                    <th>成绩</th>
                    <th>操作</th>
                  </tr>
                </thead>
                <tbody>
                    <c:forEach items="${teaScoreList}" var="score" varStatus="status">
                        <tr>
                            <td></td>
                            <td>${score.term}</td>
                            <td>${score.classNo}</td>
                            <td>${score.ID}</td>
                            <td>${score.stuName}</td>
                            <td>${score.name}</td>
                            <td class="score">${score.score}</td>
                            <td>
                              <a style="cursor: pointer" href="<%=path%>/teacher/deleteStuScore?ID=${score.ID}&courseId=${score.courseId}" onclick="return confirm('您确定要删除吗?')">删除</a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
              </table>
              <div class="offset-md-11" style="margin-top: 18px" id="avg">平均成绩：${avgScore}</div>
              <script>
                cal()
                document.onkeyup = cal
                function cal(e){
                  var avg = document.getElementById('avg')
                  var score = document.getElementsByClassName('score')

                  var sum = 0;
                  for(var i = 0; i < score.length; i++){
                    sum += parseInt(score[i].innerHTML)
                  }
                  var res = 0;
                  if(score.length > 0){
                    res = sum / score.length
                    res = res.toFixed(2)
                  }
                  else{
                    res = 0
                  }
                  avg.innerHTML = '平均成绩：' + res
                }
              </script>
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="<%= basePath%>resources/js/jquery-3.2.1.min.js"></script>
    <script src="<%= basePath%>resources/js/popper.min.js"></script>
    <script src="<%= basePath%>resources/js/bootstrap.min.js"></script>
    <script src="<%= basePath%>resources/js/main.js"></script>
    <!-- Data table plugin-->
    <script type="text/javascript" src="<%= basePath%>resources/js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<%= basePath%>resources/js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var t = $('#scoreTable').DataTable( {
                "columnDefs": [ {
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                } ],
                "order": [[ 1, 'asc' ]]
            } );

            t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();
        } );
    </script>
  </body>
</html>