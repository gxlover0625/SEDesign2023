package com.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Point2 {
    private Long bid;
    //知识点名
    private String pname;
    private Long aid;
}
