package com.service;

import com.bean.Admin;
import com.bean.Department;
import com.bean.Student;
import com.bean.Teacher;

import java.util.List;

public interface LoginService {
    Student checkStuPassword(String ID, String password);
    Teacher checkTeaPassword(String ID, String password);
    Admin checkAdmPassword(String ID, String password);

}
