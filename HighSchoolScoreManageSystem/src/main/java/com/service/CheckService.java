package com.service;

import com.bean.Check;
import com.bean.CheckRecord;

import java.util.List;

public interface CheckService {
    List<Check> getCheckList(String id);

    String getCidbyBeginTime(long beginTime);

    void insertRecord(String cid, String id);

    List<CheckRecord> getRecordList(String cid);

    List<CheckRecord> getRecordListBySno(String Sno);

    void updateRecord(String id, String cid, String cur);

    Check getCheckByCid(String cid);
}
