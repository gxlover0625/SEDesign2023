package com.interceptor;
import com.bean.Admin;
import com.bean.Student;
import com.bean.Teacher;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 权限拦截器,判断是否已经登录
 */
public class PrivilegeInterceptor implements HandlerInterceptor {

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        //逻辑：判断用户是否登录  本质：判断session中有没有user
        HttpSession session = request.getSession();
        Admin admin = (Admin) session.getAttribute("admin");
        Student student = (Student) session.getAttribute("student");
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        if(admin != null){
            //放行  访问目标资源
            return true;
        }else if(student != null){
            //放行  访问目标资源
            return true;
        }else if(teacher != null){
            //放行  访问目标资源
            return true;
        }
        //没有登录
        String contextPath = request.getContextPath();
        response.sendRedirect(contextPath + "/index.jsp");

        return false;

    }
}
