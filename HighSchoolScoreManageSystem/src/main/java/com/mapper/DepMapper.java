package com.mapper;

import com.bean.Department;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DepMapper {
    List<Department> selectDepAll();

    void insertDep(@Param("ID") String Id,@Param("name") String name);

    void deleteDep(@Param("id") String id);

    void updateDep(@Param("id") String id,@Param("name") String name);
}
