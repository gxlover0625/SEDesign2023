package com.service;

import com.bean.Score;

import java.util.List;

public interface ScoreService {
    List<Score> getScore();

    List<Score> getStuScore(String id);

    List<Score> getMyStuScore(String id);

    void removeStuScore(String id, String courseId);
}
