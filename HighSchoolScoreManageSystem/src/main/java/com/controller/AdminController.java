package com.controller;

import com.bean.*;
import com.service.*;
import com.sun.org.apache.xpath.internal.operations.Mod;
import com.sun.tracing.dtrace.Attributes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.lang.Class;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    DepService depService;

    @Autowired
    ClassService classService;

    @Autowired
    StuService stuService;

    @Autowired
    TeaService teaService;

    @Autowired
    CouService couService;

    @Autowired
    ScoreService scoreService;

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    //获取管理员个人信息
    ModelAndView getInfoPage(ModelAndView modelAndView){
        modelAndView.setViewName("admin/index");
        return modelAndView;
    }
    @RequestMapping(value = "/department", method = RequestMethod.GET)
    //获取所有专业信息
    ModelAndView getDepInfo(ModelAndView modelAndView, HttpSession session){
        if(session.getAttribute("departmentList") == null){
            List<Department> departmentList = depService.getDepInfo();
            //放到session作用域中，方便后面重复使用
            session.setAttribute("departmentList", departmentList);
        }
        modelAndView.setViewName("/admin/department");
        return modelAndView;
    }
    @RequestMapping(value = "/insertDepartment", method = RequestMethod.POST)
    //添加专业
    ModelAndView insertDep(ModelAndView modelAndView, String ID, String name, HttpServletRequest request, HttpSession session) throws UnsupportedEncodingException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        depService.addDep(ID, name);
        //删除原有的部门信息列表
        session.removeAttribute("departmentList");
        modelAndView.setViewName("redirect:" + contextPath + "/admin/department");
        return modelAndView;
    }

    @RequestMapping(value = "/deleteDep")
    //删除专业
    ModelAndView deleteDep(ModelAndView modelAndView, String ID, HttpServletRequest request, HttpSession session) throws UnsupportedEncodingException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        depService.removeDep(ID);
        session.removeAttribute("departmentList");
        session.removeAttribute("classList");
        modelAndView.setViewName("redirect:" + contextPath + "/admin/department");
        return modelAndView;
    }

    @RequestMapping(value = "/updateDep", method = RequestMethod.GET)
    //修改专业信息
    ModelAndView updateDep(ModelAndView modelAndView, String reqForward, String ID, String name, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        if("true".equals(reqForward)){
            Department updateDep = new Department();
            updateDep.setID(ID);
            updateDep.setName(name);
            session.setAttribute("updateDep", updateDep);
            session.removeAttribute("classList");
            modelAndView.setViewName("admin/updateDepartment");
            return modelAndView;
        }
        depService.updateDep(ID, name);
        session.removeAttribute("departmentList");
        modelAndView.setViewName("redirect:" + contextPath + "/admin/department");
        return modelAndView;
    }
    @RequestMapping(value = "/class", method = RequestMethod.GET)
    //获取所有班级信息
    ModelAndView getClassInfo(ModelAndView modelAndView, HttpSession session){
        if(session.getAttribute("departmentList") == null){
            List<Department> departmentList = depService.getDepInfo();
            session.setAttribute("departmentList", departmentList);
        }
        if(session.getAttribute("classList") == null){
            List<Class> classList = classService.getClassInfo();
            session.setAttribute("classList", classList);
        }
        modelAndView.setViewName("/admin/class");
        return modelAndView;
    }

    @RequestMapping(value = "/insertClass", method = RequestMethod.POST)
    //添加班级信息
    ModelAndView insertClass(ModelAndView modelAndView, String departmentID, String classID, HttpServletRequest request, HttpSession session) throws UnsupportedEncodingException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        classService.addClass(departmentID, classID);
        session.removeAttribute("classList");
        modelAndView.setViewName("redirect:"+ contextPath + "/admin/class");
        return modelAndView;
    }

    @RequestMapping(value = "/deleteClass")
    //删除班级
    ModelAndView deleteClass(ModelAndView modelAndView, String ID, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        classService.removeClass(ID);
        session.removeAttribute("classList");
        modelAndView.setViewName("redirect:" + contextPath + "/admin/class");
        return modelAndView;
    }

    @RequestMapping(value = "/updateClass", method = RequestMethod.GET)
    //修改班级信息
    ModelAndView updateClass(ModelAndView modelAndView, String reqForward, String oldId, String ID, String departmentName, String departmentId, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        if("true".equals(reqForward)){
            com.bean.Class updateClass = new com.bean.Class();
            updateClass.setID(ID);
            updateClass.setDepartmentName(departmentName);
            updateClass.setDepartmentId(departmentId);
            session.setAttribute("oldId", ID);
            session.setAttribute("updateClass", updateClass);
            modelAndView.setViewName("admin/updateClass");
            return modelAndView;
        }
        classService.updateClass(ID, departmentId, oldId);
        session.removeAttribute("classList");
        modelAndView.setViewName("redirect:" + contextPath + "/admin/class");
        return modelAndView;
    }
    @RequestMapping(value = "/student", method = RequestMethod.GET)
    //获取所有学生信息
    ModelAndView getStuInfo(ModelAndView modelAndView, HttpSession session){
        if(session.getAttribute("departmentList") == null){
            List<Department> departmentList = depService.getDepInfo();
            session.setAttribute("departmentList", departmentList);
        }
        if(session.getAttribute("classList") == null){
            List<Class> classList = classService.getClassInfo();
            session.setAttribute("classList", classList);
        }
        if(session.getAttribute("studentList") == null){
            List<Student> studentList = stuService.getStuInfo();
            session.setAttribute("studentList", studentList);
        }
        modelAndView.setViewName("/admin/student");
        return modelAndView;
    }

    @RequestMapping(value = "/insertStudent", method = RequestMethod.POST)
    //添加学生
    ModelAndView addStu(ModelAndView modelAndView, Student student, HttpServletRequest request, HttpSession session){
        stuService.addStu(student);
        session.removeAttribute("studentList");
        String contextPath = request.getContextPath();
        modelAndView.setViewName("redirect:" + contextPath + "/admin/student");
        return modelAndView;
    }

    @RequestMapping(value = "/deleteStu")
    //删除学生
    ModelAndView deleteStu(ModelAndView modelAndView, Student student, String reqForward, HttpServletRequest request, HttpSession session) throws UnsupportedEncodingException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        System.out.println(student.getID());
        stuService.removeStu(student.getID());
        session.removeAttribute("studentList");
        session.removeAttribute("scoreList");
        modelAndView.setViewName("redirect:" + contextPath + "/admin/student");
        return modelAndView;
    }

    @RequestMapping(value = "/updateStu")
    //修改学生信息
    ModelAndView updateStu(ModelAndView modelAndView, Student student, String reqForward, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        if("true".equals(reqForward)){
            student = stuService.getStuById(student.getID());
            session.setAttribute("updateStudent", student);
            modelAndView.setViewName("admin/updateStudent");
            return modelAndView;
        }
        stuService.updateStu(student);
        session.removeAttribute("studentList");
        session.removeAttribute("scoreList");
        modelAndView.setViewName("redirect:" + contextPath + "/admin/student");
        return modelAndView;
    }
    @RequestMapping(value = "/teacher", method = RequestMethod.GET)
    //获得老师列表
    ModelAndView getTeaInfo(ModelAndView modelAndView, HttpServletRequest request, HttpSession session){
        if(session.getAttribute("teacherList") == null){
            List<Teacher> teacherList = teaService.getTeaList();
            session.setAttribute("teacherList", teacherList);
        }
        modelAndView.setViewName("/admin/teacher");
        return modelAndView;
    }

    @RequestMapping(value = "/insertTeacher", method = RequestMethod.POST)
    //添加老师
    ModelAndView addTeacher(ModelAndView modelAndView, Teacher teacher, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        teaService.addTea(teacher);
        session.removeAttribute("teacherList");
        modelAndView.setViewName("redirect:"+contextPath+"/admin/teacher");
        return modelAndView;
    }

    @RequestMapping(value = "/deleteTea")
    //删除老师
    ModelAndView deleteTea(ModelAndView modelAndView, String ID, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        teaService.removeTea(ID);
        session.removeAttribute("teacherList");
        modelAndView.setViewName("redirect:" + contextPath + "/admin/teacher");
        return modelAndView;
    }
    @RequestMapping(value = "/updateTea")
    //修改教师信息
    ModelAndView updateTea(ModelAndView modelAndView, Teacher teacher, String reqForward, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        if("true".equals(reqForward)){
            teacher = teaService.getTeaById(teacher.getID());
            session.setAttribute("updateTeacher", teacher);
            modelAndView.setViewName("admin/updateTeacher");
            return modelAndView;
        }
        teaService.updateTea(teacher);
        session.removeAttribute("teacherList");
        session.removeAttribute("scoreList");
        modelAndView.setViewName("redirect:" + contextPath + "/admin/teacher");
        return modelAndView;
    }
    @RequestMapping(value = "/course", method = RequestMethod.GET)
    //获取课程列表
    ModelAndView getCourseList(ModelAndView modelAndView, HttpSession session){
        if(session.getAttribute("teacherList") == null){
            List<Teacher> teacherList = teaService.getTeaList();
            session.setAttribute("teacherList", teacherList);
        }
        if(session.getAttribute("courseList") == null){
            List<Course> courseList = couService.getCourseList();
            session.setAttribute("courseList", courseList);
        }
        modelAndView.setViewName("/admin/course");
        return modelAndView;
    }

    @RequestMapping(value = "/insertCourse", method = RequestMethod.POST)
    //添加课程
    ModelAndView addCourse(ModelAndView modelAndView, Course course, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        couService.addCourse(course);
        session.removeAttribute("courseList");
        modelAndView.setViewName("redirect:"+ contextPath + "/admin/course");
        return modelAndView;
    }

    @RequestMapping(value = "/deleteCourse")
    //删除课程
    ModelAndView deleteCourse(ModelAndView modelAndView, String ID, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        couService.removeCou(ID);
        session.removeAttribute("courseList");
        modelAndView.setViewName("redirect:" + contextPath + "/admin/course");
        return modelAndView;
    }

    @RequestMapping(value = "/updateCourse")
    //修改课程信息
    ModelAndView updateCourese(ModelAndView modelAndView, Course course, String reqForward, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        if("true".equals(reqForward)){
            course = couService.getCouById(course.getID());
            session.setAttribute("updateCou", course);
            modelAndView.setViewName("admin/updateCourse");
            return modelAndView;
        }
        couService.updateCou(course);
        session.removeAttribute("courseList");
        modelAndView.setViewName("redirect:" + contextPath + "/admin/course");
        return modelAndView;
    }
    @RequestMapping(value = "/classCourse", method = RequestMethod.GET)
    //班级开课，获取所有班级的课程
    ModelAndView getClassCourse(ModelAndView modelAndView, HttpSession session){
        if(session.getAttribute("courseList") == null){
            List<Course> courseList = couService.getCourseList();
            session.setAttribute("courseList", courseList);
        }
        if(session.getAttribute("classList") == null){
            List<Class> classList = classService.getClassInfo();
            session.setAttribute("classList", classList);
        }
        if(session.getAttribute("classCourseList") == null){
            List<Course> courseList = couService.getClassCouList();
            session.setAttribute("classCourseList", courseList);
        }
        modelAndView.setViewName("/admin/classCourse");
        return modelAndView;
    }

    @RequestMapping(value = "/insertClassCourse", method = RequestMethod.POST)
    //添加班级课程
    ModelAndView insertClassCourse(ModelAndView modelAndView, String ID, String classNo,HttpServletRequest request, HttpSession session) throws UnsupportedEncodingException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        couService.addClassCourse(classNo, ID);
        session.removeAttribute("classCourseList");
        modelAndView.setViewName("redirect:"+ contextPath + "/admin/classCourse");
        return modelAndView;
    }
    @RequestMapping(value = "/deleteClassCourse")
    //删除班级开课信息
    ModelAndView deleteClassCourse(ModelAndView modelAndView, String classNo, String ID, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        couService.removeClassCou(classNo, ID);
        session.removeAttribute("classCourseList");
        modelAndView.setViewName("redirect:" + contextPath + "/admin/classCourse");
        return modelAndView;
    }
    @RequestMapping(value = "/score", method = RequestMethod.GET)
    //查询所有成绩
    ModelAndView getScore(ModelAndView modelAndView, HttpSession session){
        if(session.getAttribute("scoreList") == null){
            List<Score> scoreList = scoreService.getScore();
            session.setAttribute("scoreList", scoreList);
        }
        modelAndView.setViewName("/admin/score");
        return modelAndView;
    }
}

