package com.service.impl;

import com.bean.Point1;
import com.bean.Point2;
import com.mapper.PointMapper;
import com.service.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PointServiceImpl implements PointService {
    @Autowired
    PointMapper pointMapper;

    @Override
    public List<Point1> findAllPoint1() {
        return pointMapper.findAllPoint1();
    }

    @Override
    public List<Point1> findAllPoint1ByTeaId(String tid) {
        return pointMapper.findAllPoint1ByTeaId(tid);
    }

    @Override
    public int insertPoint1(String cname) {
        return pointMapper.insertPoint1(cname);
    }

    @Override
    public int insertPointOwner(String cid, String tid) {
        return pointMapper.insertPointOwner(cid,tid);
    }

    @Override
    public int getLastId() {
        return pointMapper.getLastId();
    }

    @Override
    public int insertPoint2(String aid, String pname) {
        return pointMapper.insertPoint2(aid,pname);
    }

    @Override
    public int deletePoint1(String kid, String tid) {
        return pointMapper.deletePoint1(kid,tid);
    }

    @Override
    public int deletePoint2(String bid) {
        return pointMapper.deletePoint2(bid);
    }

    @Override
    public List<Point2> queryPoint2ByPoint1Id(String cid) {
        return pointMapper.queryPoint2ByPoint1Id(cid).getPoint2();
    }
}
