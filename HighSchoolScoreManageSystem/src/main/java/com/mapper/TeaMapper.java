package com.mapper;

import com.bean.MyStudent;
import com.bean.Score;
import com.bean.Student;
import com.bean.Teacher;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TeaMapper {
    List<Teacher> selectTea();

    void insertTea(Teacher teacher);

    Teacher selectTeaById(@Param("id") String id);

    List<MyStudent> selectMyStu(@Param("id") String id);

    void InsertScore(Score score);

    void deleteTea(@Param("id") String id);

    void updateTea(@Param("teacher") Teacher teacher);

    void insertPwd(@Param("teacher") Teacher teacher);

    void updatePwd(@Param("id") String id,@Param("newPwd") String newPwd);

    void insertCheck(@Param("tno") String tno, @Param("Cno") String Cno, @Param("ClassNo") String ClassNo,@Param("beginTime") long beginTime,@Param("endTime") long endTime,@Param("pwd") String pwd);
}
