package com.service.impl;

import com.bean.Score;
import com.mapper.ScoreMapper;
import com.mapper.StuMapper;
import com.service.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScoreServiceImpl implements ScoreService {
    @Autowired
    ScoreMapper scoreMapper;
    @Autowired
    StuMapper stuMapper;
    @Override
    public List<Score> getScore() {
        return scoreMapper.selectScore();
    }

    @Override
    public List<Score> getStuScore(String id) {
        return scoreMapper.selectStuScoreList(id);
    }

    @Override
    public List<Score> getMyStuScore(String id) {
        return scoreMapper.selectMyStuScore(id);
    }

    @Override
    public void removeStuScore(String id, String courseId) {
        scoreMapper.deleteStuScore(id, courseId);
        //stuMapper.updateScreditBack(id, courseId);
    }
}
