package com.mapper;

import com.bean.Point1;
import com.bean.Point2;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

import java.util.List;

public interface PointMapper {
    List<Point1> findAllPoint1();

    List<Point1> findAllPoint1ByTeaId(@Param("tid") String tid);

    int insertPoint1(@Param("cname")String cname);

    int insertPointOwner(@Param("cid")String cid, @Param("tid")String tid);

    int getLastId();

    int insertPoint2(@Param("aid")String aid,@Param("pname")String pname);

    int deletePoint1(@Param("kid")String kid,@Param("tid")String tid);

    int deletePoint2(@Param("bid")String bid);

    Point1 queryPoint2ByPoint1Id(@Param("cid")String cid);
}
