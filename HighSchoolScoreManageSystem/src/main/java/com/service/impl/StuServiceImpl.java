package com.service.impl;

import com.bean.Student;
import com.mapper.StuMapper;
import com.service.StuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StuServiceImpl implements StuService {
    @Autowired
    StuMapper stuMapper;
    @Override
    public List<Student> getStuInfo() {
        return stuMapper.selectStuList();
    }

    @Override
    public void addStu(Student student) {
        //添加学生信息
        stuMapper.insertStu(student);
        //添加学生的账户密码
        stuMapper.insertPwd(student);
    }

    @Override
    public Student getStuById(String id) {
        return stuMapper.selectStuById(id);
    }

    @Override
    public void removeStu(String id) {
        stuMapper.deleteStu(id);
    }

    @Override
    public void updateStu(Student student) {
        stuMapper.updateStu(student);
    }

    @Override
    public void updatePwd(String id, String newPwd) {
        stuMapper.updatePwd(id, newPwd);
    }


}
