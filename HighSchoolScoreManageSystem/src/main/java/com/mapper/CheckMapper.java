package com.mapper;

import com.bean.Check;
import com.bean.CheckRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CheckMapper {
    List<Check> selectCheck(@Param("id") String id);

    String selectCidByBeginTime(@Param("beginTime") long beginTime);

    void insertRecord(@Param("Cid") String cid,@Param("Sno") String sno);

    List<CheckRecord> getRecordList(@Param("cid") String cid);

    List<CheckRecord> selectRecordListBySno(@Param("Sno") String sno);

    void updateRecord(@Param("Sno") String id, @Param("Cid") String cid,
                      @Param("Cur") String cur);

    Check selectCheckByCid(@Param("cid") String cid);
}

