package com.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Point1 {
    private long id;
    //知识点名
    private String pointname;
    private List<Point2> point2;//使用一个List来表示二级目录
    private List<Teacher> teachers;
}
