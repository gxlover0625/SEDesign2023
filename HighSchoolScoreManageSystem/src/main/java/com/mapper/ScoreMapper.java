package com.mapper;

import com.bean.Score;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ScoreMapper {
    List<Score> selectScore();

    List<Score> selectStuScoreList(@Param("id") String id);

    List<Score> selectMyStuScore(@Param("id") String id);

    void deleteStuScore(@Param("id") String id,@Param("courseId") String courseId);
}
