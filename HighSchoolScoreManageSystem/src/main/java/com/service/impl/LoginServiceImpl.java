package com.service.impl;

import com.bean.Admin;
import com.bean.Department;
import com.bean.Student;
import com.bean.Teacher;
import com.mapper.LoginMapper;
import com.mapper.StuMapper;
import com.service.LoginService;
import org.apache.ibatis.executor.statement.StatementUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    LoginMapper loginMapper;
    @Override
    public Student checkStuPassword(String ID, String password) {
        return loginMapper.selectStuPwd(ID, password);
    }

    @Override
    public Teacher checkTeaPassword(String ID, String password) {
        return loginMapper.selectTeaPwd(ID, password);
    }

    @Override
    public Admin checkAdmPassword(String ID, String password) {
        return loginMapper.selectAdm(ID, password);
    }

}
