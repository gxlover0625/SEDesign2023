/*
Navicat MySQL Data Transfer

Source Server         : gx_lover
Source Server Version : 80026
Source Host           : localhost:3306
Source Database       : wangyibin05mis

Target Server Type    : MYSQL
Target Server Version : 80026
File Encoding         : 65001

Date: 2023-01-05 17:22:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `lianggb_exam_info`
-- ----------------------------
DROP TABLE IF EXISTS `lianggb_exam_info`;
CREATE TABLE `lianggb_exam_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  `examname` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `aid` int NOT NULL,
  `tid` varchar(255) NOT NULL,
  `classid` varchar(20) NOT NULL,
  `begindate` datetime NOT NULL,
  `enddate` datetime NOT NULL,
  `examtime` int DEFAULT NULL,
  `choicenum` int NOT NULL,
  `choicescore` int NOT NULL,
  `judgenum` int NOT NULL,
  `judgescore` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_exam_chapter` (`aid`),
  KEY `fk_exam_classid` (`classid`),
  KEY `fl_exam_tid` (`tid`),
  CONSTRAINT `fk_exam_chapter` FOREIGN KEY (`aid`) REFERENCES `lianggb_point1` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_exam_classid` FOREIGN KEY (`classid`) REFERENCES `wangyb_class` (`wyb_Classno`) ON UPDATE CASCADE,
  CONSTRAINT `fl_exam_tid` FOREIGN KEY (`tid`) REFERENCES `wangyb_teachers` (`wyb_Tno`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of lianggb_exam_info
-- ----------------------------
INSERT INTO `lianggb_exam_info` VALUES ('1', '软件工程期末考', '123456', '10', '1', '软件工程2002', '2023-01-05 08:00:00', '2023-01-05 10:00:00', '120', '1', '50', '1', '50');
INSERT INTO `lianggb_exam_info` VALUES ('2', '软件工程期末考3', '123456', '10', '1', '软件工程2002', '2023-01-05 13:17:00', '2023-01-05 17:17:00', '240', '1', '50', '1', '50');
INSERT INTO `lianggb_exam_info` VALUES ('3', '软件工程期末考2', '123456', '13', '1', '网络工程2002', '2023-01-05 13:20:00', '2023-01-06 13:20:00', '1440', '1', '50', '3', '50');
INSERT INTO `lianggb_exam_info` VALUES ('4', '软件工程期末考26', '123456', '14', '1', '软件工程2002', '2023-01-05 13:27:00', '2023-01-05 17:34:00', '247', '2', '50', '3', '100');
INSERT INTO `lianggb_exam_info` VALUES ('5', '软件工程期末考3', '123456', '10', '1', '软件工程2002', '2023-01-05 15:00:00', '2023-01-05 17:00:00', '120', '2', '50', '2', '50');
INSERT INTO `lianggb_exam_info` VALUES ('6', '软件工程期末考5', '123456', '10', '1', '软件工程2002', '2023-01-05 15:35:00', '2023-01-05 17:35:00', '120', '1', '50', '1', '50');
INSERT INTO `lianggb_exam_info` VALUES ('7', '软件工程期末考7', '123456', '10', '1', '软件工程2002', '2023-01-05 15:50:00', '2023-01-05 21:50:00', '360', '1', '50', '1', '50');
INSERT INTO `lianggb_exam_info` VALUES ('8', '软件工程期末考9', '123456', '10', '1', '软件工程2002', '2023-01-05 21:00:00', '2023-01-05 23:00:00', '120', '1', '50', '1', '50');
INSERT INTO `lianggb_exam_info` VALUES ('9', '软件工程期末考10', '123456', '10', '1', '软件工程2002', '2023-01-05 15:57:00', '2023-01-05 20:02:00', '245', '1', '10', '1', '50');

-- ----------------------------
-- Table structure for `lianggb_kowner`
-- ----------------------------
DROP TABLE IF EXISTS `lianggb_kowner`;
CREATE TABLE `lianggb_kowner` (
  `tid` varchar(20) NOT NULL,
  `kid` int NOT NULL,
  PRIMARY KEY (`tid`,`kid`),
  KEY `fk_kid` (`kid`),
  CONSTRAINT `fk_kid` FOREIGN KEY (`kid`) REFERENCES `lianggb_point1` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_tid` FOREIGN KEY (`tid`) REFERENCES `wangyb_teachers` (`wyb_Tno`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of lianggb_kowner
-- ----------------------------
INSERT INTO `lianggb_kowner` VALUES ('2', '1');
INSERT INTO `lianggb_kowner` VALUES ('1', '8');
INSERT INTO `lianggb_kowner` VALUES ('2', '8');
INSERT INTO `lianggb_kowner` VALUES ('1', '10');
INSERT INTO `lianggb_kowner` VALUES ('1', '13');
INSERT INTO `lianggb_kowner` VALUES ('1', '14');

-- ----------------------------
-- Table structure for `lianggb_paper_choice`
-- ----------------------------
DROP TABLE IF EXISTS `lianggb_paper_choice`;
CREATE TABLE `lianggb_paper_choice` (
  `examid` int NOT NULL,
  `tkchid` int NOT NULL,
  PRIMARY KEY (`examid`,`tkchid`),
  KEY `fk_paper_choice_id` (`tkchid`),
  CONSTRAINT `fk_paper_choice_id` FOREIGN KEY (`tkchid`) REFERENCES `lianggb_tk_choice` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_paper_exam_id` FOREIGN KEY (`examid`) REFERENCES `lianggb_exam_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of lianggb_paper_choice
-- ----------------------------
INSERT INTO `lianggb_paper_choice` VALUES ('1', '2');
INSERT INTO `lianggb_paper_choice` VALUES ('1', '3');
INSERT INTO `lianggb_paper_choice` VALUES ('9', '6');

-- ----------------------------
-- Table structure for `lianggb_paper_judge`
-- ----------------------------
DROP TABLE IF EXISTS `lianggb_paper_judge`;
CREATE TABLE `lianggb_paper_judge` (
  `examid` int NOT NULL,
  `tkjid` int NOT NULL,
  PRIMARY KEY (`examid`,`tkjid`),
  KEY `tkjid` (`tkjid`),
  CONSTRAINT `lianggb_paper_judge_ibfk_1` FOREIGN KEY (`examid`) REFERENCES `lianggb_exam_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lianggb_paper_judge_ibfk_2` FOREIGN KEY (`tkjid`) REFERENCES `lianggb_tk_judge` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of lianggb_paper_judge
-- ----------------------------
INSERT INTO `lianggb_paper_judge` VALUES ('1', '1');
INSERT INTO `lianggb_paper_judge` VALUES ('1', '21');
INSERT INTO `lianggb_paper_judge` VALUES ('9', '23');

-- ----------------------------
-- Table structure for `lianggb_point1`
-- ----------------------------
DROP TABLE IF EXISTS `lianggb_point1`;
CREATE TABLE `lianggb_point1` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pointname` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of lianggb_point1
-- ----------------------------
INSERT INTO `lianggb_point1` VALUES ('1', '软件工程');
INSERT INTO `lianggb_point1` VALUES ('2', 'C++程序设计');
INSERT INTO `lianggb_point1` VALUES ('3', '软件工程2');
INSERT INTO `lianggb_point1` VALUES ('4', '软工课设');
INSERT INTO `lianggb_point1` VALUES ('5', 'C++程序设计2');
INSERT INTO `lianggb_point1` VALUES ('6', 'test');
INSERT INTO `lianggb_point1` VALUES ('7', 'C++程序设计');
INSERT INTO `lianggb_point1` VALUES ('8', 'C++程序设计');
INSERT INTO `lianggb_point1` VALUES ('9', 'C++程序设计');
INSERT INTO `lianggb_point1` VALUES ('10', '软件工程');
INSERT INTO `lianggb_point1` VALUES ('11', 'test3');
INSERT INTO `lianggb_point1` VALUES ('12', 'test4');
INSERT INTO `lianggb_point1` VALUES ('13', 'Java课程设计');
INSERT INTO `lianggb_point1` VALUES ('14', '数据结构');

-- ----------------------------
-- Table structure for `lianggb_point2`
-- ----------------------------
DROP TABLE IF EXISTS `lianggb_point2`;
CREATE TABLE `lianggb_point2` (
  `bid` int NOT NULL AUTO_INCREMENT,
  `pname` varchar(50) NOT NULL,
  `aid` int NOT NULL,
  PRIMARY KEY (`bid`),
  KEY `fk_point_id` (`aid`),
  CONSTRAINT `fk_point_id` FOREIGN KEY (`aid`) REFERENCES `lianggb_point1` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of lianggb_point2
-- ----------------------------
INSERT INTO `lianggb_point2` VALUES ('4', '基础', '6');
INSERT INTO `lianggb_point2` VALUES ('7', '软件危机', '1');
INSERT INTO `lianggb_point2` VALUES ('8', '数组', '5');
INSERT INTO `lianggb_point2` VALUES ('9', '需求分析', '1');
INSERT INTO `lianggb_point2` VALUES ('10', '面向对象设计', '1');
INSERT INTO `lianggb_point2` VALUES ('13', '基础', '7');
INSERT INTO `lianggb_point2` VALUES ('14', '基础', '10');
INSERT INTO `lianggb_point2` VALUES ('15', '数组', '8');
INSERT INTO `lianggb_point2` VALUES ('19', '指针', '8');
INSERT INTO `lianggb_point2` VALUES ('20', 'Class类', '13');
INSERT INTO `lianggb_point2` VALUES ('21', '数据类型', '8');
INSERT INTO `lianggb_point2` VALUES ('22', 'GUI', '13');
INSERT INTO `lianggb_point2` VALUES ('23', '基础', '14');

-- ----------------------------
-- Table structure for `lianggb_tk_choice`
-- ----------------------------
DROP TABLE IF EXISTS `lianggb_tk_choice`;
CREATE TABLE `lianggb_tk_choice` (
  `id` int NOT NULL AUTO_INCREMENT,
  `content` longtext NOT NULL,
  `aoption` longtext NOT NULL,
  `boption` longtext NOT NULL,
  `coption` longtext NOT NULL,
  `doption` longtext NOT NULL,
  `answer` longtext NOT NULL,
  `analysis` longtext,
  `difficulty` varchar(255) NOT NULL,
  `bid` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_choice_bid` (`bid`),
  CONSTRAINT `fk_choice_bid` FOREIGN KEY (`bid`) REFERENCES `lianggb_point2` (`bid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of lianggb_tk_choice
-- ----------------------------
INSERT INTO `lianggb_tk_choice` VALUES ('2', '数组test', '1', '2', '3', '4', 'A', 'null', '1', '15');
INSERT INTO `lianggb_tk_choice` VALUES ('3', '222', '12', '13', '14', '15', 'C', '22222', '4', '21');
INSERT INTO `lianggb_tk_choice` VALUES ('4', '1', '3', '4', '5', '6', 'A', '2', '1', '22');
INSERT INTO `lianggb_tk_choice` VALUES ('5', '1', '3', '4', '5', '6', 'A', '2', '1', '23');
INSERT INTO `lianggb_tk_choice` VALUES ('6', '软件计划时期的主要任务是：分析用户要求、新系统的主要目标以及', '开发软件', '开发的可行性', '设计软件', '运行软件', 'B', '无', '2', '14');
INSERT INTO `lianggb_tk_choice` VALUES ('7', '一个软件从开始计划到废弃为止，称为软件的', '开发周期', '生存周期', '运行周期', '维护周期', 'B', '无', '2', '14');

-- ----------------------------
-- Table structure for `lianggb_tk_judge`
-- ----------------------------
DROP TABLE IF EXISTS `lianggb_tk_judge`;
CREATE TABLE `lianggb_tk_judge` (
  `id` int NOT NULL AUTO_INCREMENT,
  `content` longtext NOT NULL,
  `answer` varchar(10) NOT NULL,
  `analysis` longtext,
  `bid` int NOT NULL,
  `difficulty` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_bid` (`bid`),
  CONSTRAINT `fk_bid` FOREIGN KEY (`bid`) REFERENCES `lianggb_point2` (`bid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of lianggb_tk_judge
-- ----------------------------
INSERT INTO `lianggb_tk_judge` VALUES ('1', '1>2', '正确', '无', '4', '1');
INSERT INTO `lianggb_tk_judge` VALUES ('2', '1>3', '错误', '无', '10', '1');
INSERT INTO `lianggb_tk_judge` VALUES ('21', 'Java是一门面向对象的语言', '正确', '无', '20', '1');
INSERT INTO `lianggb_tk_judge` VALUES ('22', '软件缺少适当的文档资料属于软件危机现象之一', '正确', '无', '14', '1');
INSERT INTO `lianggb_tk_judge` VALUES ('23', '软件危机是20世纪60年代以前产生的', '错误', '无', '14', '1');
INSERT INTO `lianggb_tk_judge` VALUES ('24', '软件生存周期包括需求分析、系统设计、程序设计、测试、维护，五个阶段', '错误', '无', '14', '1');

-- ----------------------------
-- Table structure for `wangyb_adminpwd`
-- ----------------------------
DROP TABLE IF EXISTS `wangyb_adminpwd`;
CREATE TABLE `wangyb_adminpwd` (
  `wyb_Ano` varchar(20) NOT NULL,
  `wyb_Apwd` varchar(100) NOT NULL,
  PRIMARY KEY (`wyb_Ano`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of wangyb_adminpwd
-- ----------------------------
INSERT INTO `wangyb_adminpwd` VALUES ('admin', '123');
INSERT INTO `wangyb_adminpwd` VALUES ('asdf', '202cb962ac59075b964b07152d234b70');

-- ----------------------------
-- Table structure for `wangyb_article`
-- ----------------------------
DROP TABLE IF EXISTS `wangyb_article`;
CREATE TABLE `wangyb_article` (
  `wyb_Aid` int NOT NULL AUTO_INCREMENT,
  `wyb_Sno` varchar(20) DEFAULT NULL,
  `wyb_title` varchar(20) DEFAULT NULL,
  `wyb_content` varchar(200) DEFAULT NULL,
  `wyb_status` varchar(20) DEFAULT '0',
  `wyb_Cno` varchar(20) DEFAULT NULL,
  `wyb_Tno` varchar(20) DEFAULT NULL,
  `wyb_Dno` varchar(20) DEFAULT NULL,
  `wyb_pubdate` bigint DEFAULT NULL,
  PRIMARY KEY (`wyb_Aid`),
  KEY `article_sno` (`wyb_Sno`),
  CONSTRAINT `article_sno` FOREIGN KEY (`wyb_Sno`) REFERENCES `wangyb_students` (`wyb_Sno`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of wangyb_article
-- ----------------------------
INSERT INTO `wangyb_article` VALUES ('9', '1', '如何学Java', '多敲代码', '0', '1', '1', '1', '1670896552231');
INSERT INTO `wangyb_article` VALUES ('10', '1', '11', 'test', '0', '1', '1', '3', '1670896611401');
INSERT INTO `wangyb_article` VALUES ('11', '1', '如何学习数据结构', '从数组开始，不断深入学习，逐渐掌握线性表、二叉树等内容', '0', '8', '1', '3', '1672572979540');

-- ----------------------------
-- Table structure for `wangyb_cc`
-- ----------------------------
DROP TABLE IF EXISTS `wangyb_cc`;
CREATE TABLE `wangyb_cc` (
  `wyb_Classno` varchar(20) NOT NULL,
  `wyb_Cno` varchar(20) NOT NULL,
  PRIMARY KEY (`wyb_Classno`,`wyb_Cno`),
  KEY `fk_Cno` (`wyb_Cno`),
  CONSTRAINT `fk_classno` FOREIGN KEY (`wyb_Classno`) REFERENCES `wangyb_class` (`wyb_Classno`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Cno` FOREIGN KEY (`wyb_Cno`) REFERENCES `wangyb_courses` (`wyb_Cno`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of wangyb_cc
-- ----------------------------
INSERT INTO `wangyb_cc` VALUES ('网络工程2002', '1');
INSERT INTO `wangyb_cc` VALUES ('软件工程2002', '1');
INSERT INTO `wangyb_cc` VALUES ('软件工程2002', '2');
INSERT INTO `wangyb_cc` VALUES ('软件工程2002', '3');
INSERT INTO `wangyb_cc` VALUES ('软件工程2002', '6');
INSERT INTO `wangyb_cc` VALUES ('软件工程2002', '7');
INSERT INTO `wangyb_cc` VALUES ('软件工程2002', '8');

-- ----------------------------
-- Table structure for `wangyb_check`
-- ----------------------------
DROP TABLE IF EXISTS `wangyb_check`;
CREATE TABLE `wangyb_check` (
  `wyb_Cid` int NOT NULL AUTO_INCREMENT,
  `wyb_Tno` varchar(20) NOT NULL,
  `wyb_Cno` varchar(20) NOT NULL,
  `wyb_BeginTime` bigint NOT NULL,
  `wyb_EndTime` bigint NOT NULL,
  `wyb_Pwd` varchar(20) DEFAULT NULL,
  `wyb_ClassNo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`wyb_Cid`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of wangyb_check
-- ----------------------------
INSERT INTO `wangyb_check` VALUES ('17', '1', '2', '1670659111658', '1670659831658', '1', '软件工程2002');
INSERT INTO `wangyb_check` VALUES ('18', '1', '1', '1670761414235', '1670768074235', '1', '软件工程2002');
INSERT INTO `wangyb_check` VALUES ('19', '1', '1', '1670896464399', '1670897184399', '1', '软件工程2002');
INSERT INTO `wangyb_check` VALUES ('20', '1', '2', '1672573136552', '1672573736552', '123456', '软件工程2002');

-- ----------------------------
-- Table structure for `wangyb_checkrecords`
-- ----------------------------
DROP TABLE IF EXISTS `wangyb_checkrecords`;
CREATE TABLE `wangyb_checkrecords` (
  `wyb_Cid` int DEFAULT NULL,
  `wyb_Sno` varchar(20) DEFAULT NULL,
  `wyb_Status` int DEFAULT '0',
  `wyb_CheckTime` bigint DEFAULT NULL,
  KEY `Cid` (`wyb_Cid`),
  CONSTRAINT `Cid` FOREIGN KEY (`wyb_Cid`) REFERENCES `wangyb_check` (`wyb_Cid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of wangyb_checkrecords
-- ----------------------------
INSERT INTO `wangyb_checkrecords` VALUES ('17', '1', '1', '1670762328606');
INSERT INTO `wangyb_checkrecords` VALUES ('17', '2', '1', '1670762326049');
INSERT INTO `wangyb_checkrecords` VALUES ('17', '4', '1', '1670762327501');
INSERT INTO `wangyb_checkrecords` VALUES ('18', '1', '1', '1670761434285');
INSERT INTO `wangyb_checkrecords` VALUES ('18', '2', '1', '1670761459059');
INSERT INTO `wangyb_checkrecords` VALUES ('18', '4', '0', null);
INSERT INTO `wangyb_checkrecords` VALUES ('19', '1', '1', '1670896489806');
INSERT INTO `wangyb_checkrecords` VALUES ('19', '2', '0', null);
INSERT INTO `wangyb_checkrecords` VALUES ('19', '4', '1', '1670896472940');
INSERT INTO `wangyb_checkrecords` VALUES ('20', '1', '1', '1672573217002');
INSERT INTO `wangyb_checkrecords` VALUES ('20', '2', '1', '1672573602297');
INSERT INTO `wangyb_checkrecords` VALUES ('20', '4', '0', null);

-- ----------------------------
-- Table structure for `wangyb_class`
-- ----------------------------
DROP TABLE IF EXISTS `wangyb_class`;
CREATE TABLE `wangyb_class` (
  `wyb_Classno` varchar(20) NOT NULL,
  `wyb_Dno` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`wyb_Classno`),
  KEY `index_Class_Dno` (`wyb_Dno`),
  CONSTRAINT `fk_Dno` FOREIGN KEY (`wyb_Dno`) REFERENCES `wangyb_department` (`wyb_Dno`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of wangyb_class
-- ----------------------------
INSERT INTO `wangyb_class` VALUES ('测试用例2', null);
INSERT INTO `wangyb_class` VALUES ('软件工程2002', '1');
INSERT INTO `wangyb_class` VALUES ('软件工程2005', '1');
INSERT INTO `wangyb_class` VALUES ('网工2003', '2');
INSERT INTO `wangyb_class` VALUES ('网络工程2002', '2');
INSERT INTO `wangyb_class` VALUES ('计算机科学与技术2001', '3');
INSERT INTO `wangyb_class` VALUES ('1234', '4');
INSERT INTO `wangyb_class` VALUES ('工商管理2001', '4');
INSERT INTO `wangyb_class` VALUES ('物联网2002', '5');
INSERT INTO `wangyb_class` VALUES ('大数据2002', '6');
INSERT INTO `wangyb_class` VALUES ('大数据2004', '6');
INSERT INTO `wangyb_class` VALUES ('大数据2005', '6');

-- ----------------------------
-- Table structure for `wangyb_courses`
-- ----------------------------
DROP TABLE IF EXISTS `wangyb_courses`;
CREATE TABLE `wangyb_courses` (
  `wyb_Cno` varchar(20) NOT NULL,
  `wyb_Cname` varchar(20) NOT NULL,
  `wyb_Cteacher` varchar(20) DEFAULT NULL,
  `wyb_Cterm` varchar(20) NOT NULL,
  `wyb_Chours` int NOT NULL,
  `wyb_Ctype` varchar(10) NOT NULL,
  `wyb_Ccredit` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`wyb_Cno`),
  KEY `index_Courses_Cteacher` (`wyb_Cteacher`),
  CONSTRAINT `fk_Tno` FOREIGN KEY (`wyb_Cteacher`) REFERENCES `wangyb_teachers` (`wyb_Tno`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of wangyb_courses
-- ----------------------------
INSERT INTO `wangyb_courses` VALUES ('1', '数据库原理及应用', '1', '2021-2022(2)', '48', '考试', '3');
INSERT INTO `wangyb_courses` VALUES ('2', '计算机网络原理', '1', '2021-2022(1)', '48', '考试', '3');
INSERT INTO `wangyb_courses` VALUES ('3', '前端开发', '2', '2020-2021(2)', '48', '考查', '3');
INSERT INTO `wangyb_courses` VALUES ('4', '计算机组成原理', '1', '2021-2022(1)', '48', '考试', '3');
INSERT INTO `wangyb_courses` VALUES ('5', '人工智能导论', '1', '2021-2022(2)', '48', '考试', '3');
INSERT INTO `wangyb_courses` VALUES ('6', '算法设计与实现', '1', '2021-2022(1)', '48', '考试', '3');
INSERT INTO `wangyb_courses` VALUES ('7', 'Java程序设计', '3', '2020-2021(2)', '48', '考试', '3');
INSERT INTO `wangyb_courses` VALUES ('8', '数据结构', '1', '2021-2022(1)', '48', '考试', '3');

-- ----------------------------
-- Table structure for `wangyb_department`
-- ----------------------------
DROP TABLE IF EXISTS `wangyb_department`;
CREATE TABLE `wangyb_department` (
  `wyb_Dno` varchar(20) NOT NULL,
  `wyb_Dname` varchar(20) NOT NULL,
  PRIMARY KEY (`wyb_Dno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of wangyb_department
-- ----------------------------
INSERT INTO `wangyb_department` VALUES ('1', '软件工程');
INSERT INTO `wangyb_department` VALUES ('2', '网络工程');
INSERT INTO `wangyb_department` VALUES ('3', '计算机科学与技术');
INSERT INTO `wangyb_department` VALUES ('4', '工商管理');
INSERT INTO `wangyb_department` VALUES ('5', '物联网');
INSERT INTO `wangyb_department` VALUES ('6', '大数据');
INSERT INTO `wangyb_department` VALUES ('7', '数字媒体工程');

-- ----------------------------
-- Table structure for `wangyb_sc`
-- ----------------------------
DROP TABLE IF EXISTS `wangyb_sc`;
CREATE TABLE `wangyb_sc` (
  `wyb_Sno` varchar(20) NOT NULL,
  `wyb_Cno` varchar(20) NOT NULL,
  `wyb_Cstatus` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`wyb_Sno`,`wyb_Cno`),
  KEY `sc_fk_Cno` (`wyb_Cno`),
  CONSTRAINT `sc_fk_Cno` FOREIGN KEY (`wyb_Cno`) REFERENCES `wangyb_courses` (`wyb_Cno`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sc_fk_Sno` FOREIGN KEY (`wyb_Sno`) REFERENCES `wangyb_students` (`wyb_Sno`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of wangyb_sc
-- ----------------------------
INSERT INTO `wangyb_sc` VALUES ('1', '1', '在读');
INSERT INTO `wangyb_sc` VALUES ('1', '2', '在读');
INSERT INTO `wangyb_sc` VALUES ('1', '3', '在读');
INSERT INTO `wangyb_sc` VALUES ('1', '6', '在读');
INSERT INTO `wangyb_sc` VALUES ('1', '7', '在读');
INSERT INTO `wangyb_sc` VALUES ('2', '1', '在读');
INSERT INTO `wangyb_sc` VALUES ('2', '2', '在读');
INSERT INTO `wangyb_sc` VALUES ('2', '6', '在读');
INSERT INTO `wangyb_sc` VALUES ('4', '1', '在读');
INSERT INTO `wangyb_sc` VALUES ('4', '2', '在读');
INSERT INTO `wangyb_sc` VALUES ('4', '6', '在读');

-- ----------------------------
-- Table structure for `wangyb_score`
-- ----------------------------
DROP TABLE IF EXISTS `wangyb_score`;
CREATE TABLE `wangyb_score` (
  `wyb_Sno` varchar(20) NOT NULL,
  `wyb_Cno` varchar(20) NOT NULL,
  `wyb_Tno` varchar(20) DEFAULT NULL,
  `wyb_term` varchar(20) DEFAULT NULL,
  `wyb_Score` int NOT NULL,
  PRIMARY KEY (`wyb_Sno`,`wyb_Cno`),
  KEY `score_fk_Cno` (`wyb_Cno`),
  KEY `score_fk_Tno` (`wyb_Tno`),
  CONSTRAINT `score_fk_Cno` FOREIGN KEY (`wyb_Cno`) REFERENCES `wangyb_courses` (`wyb_Cno`) ON UPDATE CASCADE,
  CONSTRAINT `score_fk_Sno` FOREIGN KEY (`wyb_Sno`) REFERENCES `wangyb_students` (`wyb_Sno`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of wangyb_score
-- ----------------------------
INSERT INTO `wangyb_score` VALUES ('1', '1', '1', '2021-2022(2)', '92');
INSERT INTO `wangyb_score` VALUES ('1', '2', '1', '2021-2022(1)', '88');
INSERT INTO `wangyb_score` VALUES ('1', '6', '1', '2021-2022(1)', '89');
INSERT INTO `wangyb_score` VALUES ('1', '7', '3', '2020-2021(2)', '98');
INSERT INTO `wangyb_score` VALUES ('2', '2', '1', '2021-2022(1)', '91');
INSERT INTO `wangyb_score` VALUES ('2', '6', '1', '2021-2022(1)', '100');
INSERT INTO `wangyb_score` VALUES ('4', '1', '1', '2021-2022(2)', '100');

-- ----------------------------
-- Table structure for `wangyb_students`
-- ----------------------------
DROP TABLE IF EXISTS `wangyb_students`;
CREATE TABLE `wangyb_students` (
  `wyb_Sno` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `wyb_Sname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `wyb_Dno` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `wyb_Classno` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `wyb_Ssex` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `wyb_Sage` int NOT NULL,
  `wyb_Saddress` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `wyb_Scredit` int DEFAULT '0',
  PRIMARY KEY (`wyb_Sno`),
  KEY `index_Students_Classno` (`wyb_Classno`),
  KEY `index_Students_Dno` (`wyb_Dno`),
  CONSTRAINT `stu_fk_classno` FOREIGN KEY (`wyb_Classno`) REFERENCES `wangyb_class` (`wyb_Classno`) ON UPDATE CASCADE,
  CONSTRAINT `stu_fk_Dno` FOREIGN KEY (`wyb_Dno`) REFERENCES `wangyb_department` (`wyb_Dno`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of wangyb_students
-- ----------------------------
INSERT INTO `wangyb_students` VALUES ('1', '王逸彬', '1', '软件工程2002', '女', '20', '浙江温州', '15');
INSERT INTO `wangyb_students` VALUES ('11', '11', null, null, '男', '11', '11', '0');
INSERT INTO `wangyb_students` VALUES ('2', '包欣蓉', '1', '软件工程2002', '女', '20', '浙江丽水', '6');
INSERT INTO `wangyb_students` VALUES ('4', '陈佳怡', '1', '软件工程2002', '女', '21', '浙江', '3');

-- ----------------------------
-- Table structure for `wangyb_stupwd`
-- ----------------------------
DROP TABLE IF EXISTS `wangyb_stupwd`;
CREATE TABLE `wangyb_stupwd` (
  `wyb_Sno` varchar(20) NOT NULL,
  `wyb_Spwd` varchar(20) NOT NULL,
  PRIMARY KEY (`wyb_Sno`),
  CONSTRAINT `stu_fk_Sno` FOREIGN KEY (`wyb_Sno`) REFERENCES `wangyb_students` (`wyb_Sno`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of wangyb_stupwd
-- ----------------------------
INSERT INTO `wangyb_stupwd` VALUES ('1', '123');
INSERT INTO `wangyb_stupwd` VALUES ('11', '123');
INSERT INTO `wangyb_stupwd` VALUES ('2', '123');
INSERT INTO `wangyb_stupwd` VALUES ('4', '123');

-- ----------------------------
-- Table structure for `wangyb_teachers`
-- ----------------------------
DROP TABLE IF EXISTS `wangyb_teachers`;
CREATE TABLE `wangyb_teachers` (
  `wyb_Tno` varchar(20) NOT NULL,
  `wyb_Tname` varchar(20) NOT NULL,
  `wyb_Tsex` varchar(2) NOT NULL,
  `wyb_Tage` int NOT NULL,
  `wyb_Ttitle` varchar(20) NOT NULL,
  `wyb_Tphone` varchar(20) NOT NULL,
  PRIMARY KEY (`wyb_Tno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of wangyb_teachers
-- ----------------------------
INSERT INTO `wangyb_teachers` VALUES ('1', '董天阳', '男', '30', '教授', '18858823413');
INSERT INTO `wangyb_teachers` VALUES ('2', '于明远', '男', '40', '教授', '18858718185');
INSERT INTO `wangyb_teachers` VALUES ('3', '张端', '男', '30', '教授', '1234145234');

-- ----------------------------
-- Table structure for `wangyb_teapwd`
-- ----------------------------
DROP TABLE IF EXISTS `wangyb_teapwd`;
CREATE TABLE `wangyb_teapwd` (
  `wyb_Tno` varchar(20) NOT NULL,
  `wyb_Tpwd` varchar(20) NOT NULL,
  PRIMARY KEY (`wyb_Tno`),
  CONSTRAINT `tea_fk_Tno` FOREIGN KEY (`wyb_Tno`) REFERENCES `wangyb_teachers` (`wyb_Tno`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of wangyb_teapwd
-- ----------------------------
INSERT INTO `wangyb_teapwd` VALUES ('1', '123');
INSERT INTO `wangyb_teapwd` VALUES ('2', '123');
INSERT INTO `wangyb_teapwd` VALUES ('3', '123');

-- ----------------------------
-- View structure for `article_view`
-- ----------------------------
DROP VIEW IF EXISTS `article_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `article_view` AS select `a`.`wyb_Aid` AS `wyb_Aid`,`a`.`wyb_Sno` AS `wyb_Sno`,`s`.`wyb_Sname` AS `wyb_Sname`,`s`.`wyb_Classno` AS `wyb_Classno`,`a`.`wyb_title` AS `wyb_title`,`a`.`wyb_content` AS `wyb_content`,`a`.`wyb_status` AS `wyb_Status`,`a`.`wyb_Cno` AS `wyb_Cno`,`c`.`wyb_Cname` AS `wyb_Cname`,`a`.`wyb_Tno` AS `wyb_Tno`,`a`.`wyb_Dno` AS `wyb_Dno`,`d`.`wyb_Dname` AS `wyb_Dname`,`t`.`wyb_Tname` AS `wyb_Tname`,`a`.`wyb_pubdate` AS `wyb_pubdate` from ((((`wangyb_article` `a` join `wangyb_students` `s`) join `wangyb_courses` `c`) join `wangyb_teachers` `t`) join `wangyb_department` `d`) where ((`a`.`wyb_Sno` = `s`.`wyb_Sno`) and (`a`.`wyb_Tno` = `t`.`wyb_Tno`) and (`a`.`wyb_Dno` = `d`.`wyb_Dno`) and (`a`.`wyb_Cno` = `c`.`wyb_Cno`)) ;

-- ----------------------------
-- View structure for `checkrecords_view`
-- ----------------------------
DROP VIEW IF EXISTS `checkrecords_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `checkrecords_view` AS select `c`.`wyb_Cid` AS `wyb_Cid`,`t`.`wyb_Tname` AS `wyb_Tname`,`cou`.`wyb_Cname` AS `wyb_Cname`,`s`.`wyb_Sname` AS `wyb_Sname`,`s`.`wyb_Sno` AS `wyb_Sno`,`cr`.`wyb_Status` AS `wyb_Status`,`c`.`wyb_EndTime` AS `wyb_EndTime`,`cr`.`wyb_CheckTime` AS `wyb_CheckTime` from ((((`wangyb_check` `c` join `wangyb_checkrecords` `cr`) join `wangyb_students` `s`) join `wangyb_teachers` `t`) join `wangyb_courses` `cou`) where ((`c`.`wyb_Tno` = `t`.`wyb_Tno`) and (`c`.`wyb_Cid` = `cr`.`wyb_Cid`) and (`c`.`wyb_Cno` = `cou`.`wyb_Cno`) and (`cr`.`wyb_Sno` = `s`.`wyb_Sno`)) ;

-- ----------------------------
-- View structure for `check_view`
-- ----------------------------
DROP VIEW IF EXISTS `check_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `check_view` AS select `c`.`wyb_Cid` AS `wyb_Cid`,`c`.`wyb_Tno` AS `wyb_Tno`,`t`.`wyb_Tname` AS `wyb_Tname`,`cou`.`wyb_Cname` AS `wyb_Cname`,`c`.`wyb_ClassNo` AS `wyb_ClassNo`,`c`.`wyb_BeginTime` AS `wyb_BeginTime`,`c`.`wyb_EndTime` AS `wyb_EndTime`,`c`.`wyb_Pwd` AS `wyb_Pwd` from ((`wangyb_check` `c` join `wangyb_teachers` `t`) join `wangyb_courses` `cou`) where ((`c`.`wyb_Tno` = `t`.`wyb_Tno`) and (`c`.`wyb_Cno` = `cou`.`wyb_Cno`)) ;

-- ----------------------------
-- View structure for `classcourse_view`
-- ----------------------------
DROP VIEW IF EXISTS `classcourse_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `classcourse_view` AS select `wangyb_cc`.`wyb_Cno` AS `wyb_Cno`,`wangyb_cc`.`wyb_Classno` AS `wyb_Classno`,`wangyb_courses`.`wyb_Cterm` AS `wyb_Cterm`,`wangyb_courses`.`wyb_Cname` AS `wyb_Cname`,`wangyb_teachers`.`wyb_Tname` AS `wyb_Tname`,`wangyb_courses`.`wyb_Chours` AS `wyb_Chours`,`wangyb_courses`.`wyb_Ccredit` AS `wyb_Ccredit`,`wangyb_courses`.`wyb_Ctype` AS `wyb_Ctype` from (((`wangyb_cc` join `wangyb_courses`) join `wangyb_teachers`) join `wangyb_class`) where ((`wangyb_courses`.`wyb_Cno` = `wangyb_cc`.`wyb_Cno`) and (`wangyb_class`.`wyb_Classno` = `wangyb_cc`.`wyb_Classno`) and (`wangyb_courses`.`wyb_Cteacher` = `wangyb_teachers`.`wyb_Tno`)) ;

-- ----------------------------
-- View structure for `class_view`
-- ----------------------------
DROP VIEW IF EXISTS `class_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `class_view` AS select `wangyb_class`.`wyb_Classno` AS `wyb_Classno`,`wangyb_department`.`wyb_Dname` AS `wyb_Dname` from (`wangyb_class` join `wangyb_department`) where (`wangyb_class`.`wyb_Dno` = `wangyb_department`.`wyb_Dno`) ;

-- ----------------------------
-- View structure for `lianggb_judge_viewfinal`
-- ----------------------------
DROP VIEW IF EXISTS `lianggb_judge_viewfinal`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lianggb_judge_viewfinal` AS select `v`.`cid` AS `cid`,`v`.`cname` AS `cname`,`v`.`pid` AS `pid`,`v`.`pname` AS `pname`,`j`.`id` AS `jid`,`j`.`content` AS `content`,`j`.`answer` AS `ans`,`j`.`analysis` AS `analysis`,`j`.`difficulty` AS `difficulty`,`v`.`tid` AS `tid`,`v`.`tname` AS `tname` from (`lianggb_point2_view` `v` join `lianggb_tk_judge` `j`) where (`v`.`pid` = `j`.`bid`) ;

-- ----------------------------
-- View structure for `lianggb_kview`
-- ----------------------------
DROP VIEW IF EXISTS `lianggb_kview`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lianggb_kview` AS select `c`.`id` AS `cid`,`c`.`pointname` AS `cname`,`p`.`bid` AS `pid`,`p`.`pname` AS `pname`,`t`.`wyb_Tno` AS `tid`,`t`.`wyb_Tname` AS `tname` from (((`lianggb_point1` `c` join `wangyb_teachers` `t`) join `lianggb_kowner` `ko`) join `lianggb_point2` `p`) where ((`c`.`id` = `ko`.`kid`) and (`t`.`wyb_Tno` = `ko`.`tid`) and (`p`.`aid` = `c`.`id`)) ;

-- ----------------------------
-- View structure for `lianggb_paper_choice_view`
-- ----------------------------
DROP VIEW IF EXISTS `lianggb_paper_choice_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lianggb_paper_choice_view` AS select `ch`.`id` AS `id`,`ch`.`content` AS `content`,`ch`.`aoption` AS `A`,`ch`.`boption` AS `B`,`ch`.`coption` AS `C`,`ch`.`doption` AS `D`,`ch`.`answer` AS `answer`,`e`.`id` AS `eid` from ((`lianggb_exam_info` `e` join `lianggb_tk_choice` `ch`) join `lianggb_paper_choice` `pc`) where ((`e`.`id` = `pc`.`examid`) and (`ch`.`id` = `pc`.`tkchid`)) ;

-- ----------------------------
-- View structure for `lianggb_paper_judge_view`
-- ----------------------------
DROP VIEW IF EXISTS `lianggb_paper_judge_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lianggb_paper_judge_view` AS select `tk`.`id` AS `tid`,`tk`.`content` AS `content`,`tk`.`answer` AS `answer`,`e`.`id` AS `eid` from ((`lianggb_exam_info` `e` join `lianggb_paper_judge` `pj`) join `lianggb_tk_judge` `tk`) where ((`e`.`id` = `pj`.`examid`) and (`pj`.`tkjid` = `tk`.`id`)) ;

-- ----------------------------
-- View structure for `lianggb_point1_view`
-- ----------------------------
DROP VIEW IF EXISTS `lianggb_point1_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lianggb_point1_view` AS select `c`.`id` AS `cid`,`c`.`pointname` AS `cname`,`t`.`wyb_Tno` AS `tid`,`t`.`wyb_Tname` AS `tname` from ((`lianggb_point1` `c` join `wangyb_teachers` `t`) join `lianggb_kowner` `ko`) where ((`c`.`id` = `ko`.`kid`) and (`t`.`wyb_Tno` = `ko`.`tid`)) ;

-- ----------------------------
-- View structure for `lianggb_point2_view`
-- ----------------------------
DROP VIEW IF EXISTS `lianggb_point2_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lianggb_point2_view` AS select `vi`.`cid` AS `cid`,`vi`.`cname` AS `cname`,`p`.`bid` AS `pid`,`p`.`pname` AS `pname`,`vi`.`tid` AS `tid`,`vi`.`tname` AS `tname` from (`lianggb_point1_view` `vi` left join `lianggb_point2` `p` on((`p`.`aid` = `vi`.`cid`))) ;

-- ----------------------------
-- View structure for `lianggb_small_choice`
-- ----------------------------
DROP VIEW IF EXISTS `lianggb_small_choice`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lianggb_small_choice` AS select `c`.`id` AS `cid`,`c`.`pointname` AS `chapter`,`choice`.`id` AS `id`,`choice`.`content` AS `content`,`choice`.`aoption` AS `aoption`,`choice`.`boption` AS `boption`,`choice`.`coption` AS `coption`,`choice`.`doption` AS `doption`,`choice`.`answer` AS `answer`,`choice`.`analysis` AS `analysis`,`choice`.`difficulty` AS `difficulty`,`choice`.`bid` AS `bid` from ((`lianggb_tk_choice` `choice` join `lianggb_point1` `c`) join `lianggb_point2` `p`) where ((`c`.`id` = `p`.`aid`) and (`p`.`bid` = `choice`.`bid`)) ;

-- ----------------------------
-- View structure for `lianggb_small_judge`
-- ----------------------------
DROP VIEW IF EXISTS `lianggb_small_judge`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lianggb_small_judge` AS select `judge`.`id` AS `id`,`judge`.`content` AS `content`,`judge`.`answer` AS `answer`,`judge`.`analysis` AS `analysis`,`judge`.`bid` AS `bid`,`judge`.`difficulty` AS `difficulty`,`c`.`id` AS `cid`,`c`.`pointname` AS `cname` from ((`lianggb_point1` `c` join `lianggb_point2` `p`) join `lianggb_tk_judge` `judge`) where ((`c`.`id` = `p`.`aid`) and (`p`.`bid` = `judge`.`bid`)) ;

-- ----------------------------
-- View structure for `lianggb_tk_choice_view`
-- ----------------------------
DROP VIEW IF EXISTS `lianggb_tk_choice_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lianggb_tk_choice_view` AS select `v`.`cid` AS `cid`,`v`.`cname` AS `cname`,`v`.`pid` AS `pid`,`v`.`pname` AS `pname`,`v`.`tid` AS `tid`,`v`.`tname` AS `tname`,`ch`.`id` AS `chid`,`ch`.`content` AS `content`,`ch`.`answer` AS `ans`,`ch`.`aoption` AS `A`,`ch`.`boption` AS `B`,`ch`.`coption` AS `C`,`ch`.`doption` AS `D`,`ch`.`analysis` AS `analysis`,`ch`.`difficulty` AS `difficulty` from (`lianggb_point2_view` `v` join `lianggb_tk_choice` `ch`) where (`ch`.`bid` = `v`.`pid`) ;

-- ----------------------------
-- View structure for `lianggb_tk_judge_view`
-- ----------------------------
DROP VIEW IF EXISTS `lianggb_tk_judge_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lianggb_tk_judge_view` AS select `c`.`id` AS `cid`,`c`.`pointname` AS `cname`,`p`.`aid` AS `pid`,`p`.`pname` AS `pname`,`j`.`id` AS `jid`,`j`.`content` AS `content`,`j`.`answer` AS `ans`,`j`.`analysis` AS `analysis`,`j`.`difficulty` AS `difficulty`,`t`.`wyb_Tno` AS `tid`,`t`.`wyb_Tname` AS `tname` from ((((`lianggb_point1` `c` join `lianggb_point2` `p`) join `lianggb_tk_judge` `j`) join `lianggb_kowner` `k`) join `wangyb_teachers` `t`) where ((`c`.`id` = `p`.`bid`) and (`j`.`bid` = `p`.`bid`) and (`k`.`kid` = `c`.`id`) and (`k`.`tid` = `t`.`wyb_Tno`)) ;

-- ----------------------------
-- View structure for `liang_exam_teacher_view`
-- ----------------------------
DROP VIEW IF EXISTS `liang_exam_teacher_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `liang_exam_teacher_view` AS select `e`.`id` AS `id`,`e`.`examname` AS `examname`,`e`.`password` AS `password`,`e`.`aid` AS `aid`,`e`.`tid` AS `tid`,`e`.`classid` AS `classid`,`e`.`begindate` AS `begindate`,`e`.`enddate` AS `enddate`,`e`.`examtime` AS `examtime`,`e`.`choicenum` AS `choicenum`,`e`.`choicescore` AS `choicescore`,`e`.`judgenum` AS `judgenum`,`e`.`judgescore` AS `judgescore`,`t`.`wyb_Tname` AS `wyb_Tname`,`c`.`pointname` AS `chapter` from ((`lianggb_exam_info` `e` join `wangyb_teachers` `t`) join `lianggb_point1` `c`) where ((`e`.`tid` = `t`.`wyb_Tno`) and (`c`.`id` = `e`.`aid`)) ;

-- ----------------------------
-- View structure for `liang_exam_view`
-- ----------------------------
DROP VIEW IF EXISTS `liang_exam_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `liang_exam_view` AS select `e`.`id` AS `id`,`e`.`examname` AS `examname`,`e`.`password` AS `password`,`e`.`aid` AS `aid`,`e`.`tid` AS `tid`,`e`.`classid` AS `classid`,`e`.`begindate` AS `begindate`,`e`.`enddate` AS `enddate`,`e`.`examtime` AS `examtime`,`e`.`choicenum` AS `choicenum`,`e`.`choicescore` AS `choicescore`,`e`.`judgenum` AS `judgenum`,`e`.`judgescore` AS `judgescore`,`v`.`wyb_sno` AS `wyb_sno`,`v`.`wyb_sname` AS `wyb_sname`,`t`.`wyb_Tname` AS `wyb_Tname`,`p`.`pointname` AS `chapter` from (((`lianggb_exam_info` `e` join `stuinfo_view` `v`) join `wangyb_teachers` `t`) join `lianggb_point1` `p`) where ((`e`.`classid` = `v`.`wyb_classno`) and (`t`.`wyb_Tno` = `e`.`tid`) and (`p`.`id` = `e`.`aid`)) ;

-- ----------------------------
-- View structure for `mystudents_view`
-- ----------------------------
DROP VIEW IF EXISTS `mystudents_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mystudents_view` AS select `wangyb_sc`.`wyb_Sno` AS `wyb_Sno`,`wangyb_teachers`.`wyb_Tno` AS `wyb_Tno`,`wangyb_students`.`wyb_Sname` AS `wyb_Sname`,`wangyb_students`.`wyb_Classno` AS `wyb_Classno`,`wangyb_courses`.`wyb_Cterm` AS `wyb_Cterm`,`wangyb_courses`.`wyb_Cname` AS `wyb_Cname`,`wangyb_sc`.`wyb_Cno` AS `wyb_Cno` from (((`wangyb_students` join `wangyb_courses`) join `wangyb_teachers`) join `wangyb_sc`) where ((`wangyb_sc`.`wyb_Sno` = `wangyb_students`.`wyb_Sno`) and (`wangyb_sc`.`wyb_Cno` = `wangyb_courses`.`wyb_Cno`) and (`wangyb_courses`.`wyb_Cteacher` = `wangyb_teachers`.`wyb_Tno`)) ;

-- ----------------------------
-- View structure for `score_view`
-- ----------------------------
DROP VIEW IF EXISTS `score_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `score_view` AS select `wangyb_students`.`wyb_Sno` AS `wyb_Sno`,`wangyb_score`.`wyb_Tno` AS `wyb_Tno`,`wangyb_students`.`wyb_Sname` AS `wyb_Sname`,`wangyb_students`.`wyb_Classno` AS `wyb_Classno`,`wangyb_score`.`wyb_term` AS `wyb_term`,`wangyb_courses`.`wyb_Cname` AS `wyb_Cname`,`wangyb_score`.`wyb_Cno` AS `wyb_Cno`,`wangyb_teachers`.`wyb_Tname` AS `wyb_Tname`,`wangyb_score`.`wyb_Score` AS `wyb_score` from (((`wangyb_score` join `wangyb_students`) join `wangyb_courses`) join `wangyb_teachers`) where ((`wangyb_score`.`wyb_Sno` = `wangyb_students`.`wyb_Sno`) and (`wangyb_score`.`wyb_Cno` = `wangyb_courses`.`wyb_Cno`) and (`wangyb_score`.`wyb_Tno` = `wangyb_teachers`.`wyb_Tno`)) ;

-- ----------------------------
-- View structure for `sc_view`
-- ----------------------------
DROP VIEW IF EXISTS `sc_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sc_view` AS select `wangyb_sc`.`wyb_Sno` AS `wyb_Sno`,`wangyb_courses`.`wyb_Cterm` AS `wyb_Cterm`,`wangyb_courses`.`wyb_Cname` AS `wyb_Cname`,`wangyb_teachers`.`wyb_Tname` AS `wyb_Tname`,`wangyb_courses`.`wyb_Chours` AS `wyb_Chours`,`wangyb_courses`.`wyb_Ccredit` AS `wyb_Ccredit`,`wangyb_courses`.`wyb_Ctype` AS `wyb_Ctype`,`wangyb_sc`.`wyb_Cstatus` AS `wyb_Cstatus` from ((`wangyb_sc` join `wangyb_courses`) join `wangyb_teachers`) where ((`wangyb_courses`.`wyb_Cno` = `wangyb_sc`.`wyb_Cno`) and (`wangyb_courses`.`wyb_Cteacher` = `wangyb_teachers`.`wyb_Tno`)) ;

-- ----------------------------
-- View structure for `stuinfo_view`
-- ----------------------------
DROP VIEW IF EXISTS `stuinfo_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `stuinfo_view` AS select `wangyb_students`.`wyb_Sno` AS `wyb_sno`,`wangyb_students`.`wyb_Sname` AS `wyb_sname`,`wangyb_students`.`wyb_Dno` AS `wyb_dno`,`wangyb_department`.`wyb_Dname` AS `wyb_Dname`,`wangyb_students`.`wyb_Classno` AS `wyb_classno`,`wangyb_students`.`wyb_Ssex` AS `wyb_ssex`,`wangyb_students`.`wyb_Sage` AS `wyb_sage`,`wangyb_students`.`wyb_Saddress` AS `wyb_saddress`,`wangyb_students`.`wyb_Scredit` AS `wyb_scredit` from (`wangyb_students` join `wangyb_department`) where (`wangyb_students`.`wyb_Dno` = `wangyb_department`.`wyb_Dno`) ;

-- ----------------------------
-- View structure for `stuscore_view`
-- ----------------------------
DROP VIEW IF EXISTS `stuscore_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `stuscore_view` AS select `wangyb_score`.`wyb_Sno` AS `wyb_Sno`,`wangyb_students`.`wyb_Sname` AS `wyb_Sname`,`wangyb_courses`.`wyb_Cterm` AS `wyb_Cterm`,`wangyb_students`.`wyb_Classno` AS `wyb_Classno`,`wangyb_courses`.`wyb_Cname` AS `wyb_Cname`,`wangyb_teachers`.`wyb_Tname` AS `wyb_Tname`,`wangyb_courses`.`wyb_Chours` AS `wyb_Chours`,`wangyb_courses`.`wyb_Ccredit` AS `wyb_Ccredit`,`wangyb_courses`.`wyb_Ctype` AS `wyb_Ctype`,`wangyb_score`.`wyb_Score` AS `wyb_Score` from (((`wangyb_score` join `wangyb_courses`) join `wangyb_teachers`) join `wangyb_students`) where ((`wangyb_courses`.`wyb_Cno` = `wangyb_score`.`wyb_Cno`) and (`wangyb_score`.`wyb_Tno` = `wangyb_teachers`.`wyb_Tno`) and (`wangyb_score`.`wyb_Sno` = `wangyb_students`.`wyb_Sno`)) ;

-- ----------------------------
-- View structure for `tc_view`
-- ----------------------------
DROP VIEW IF EXISTS `tc_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tc_view` AS select `wangyb_courses`.`wyb_Cteacher` AS `wyb_Cteacher`,`wangyb_courses`.`wyb_Cno` AS `wyb_Cno`,`wangyb_courses`.`wyb_Cterm` AS `wyb_Cterm`,`wangyb_cc`.`wyb_Classno` AS `wyb_Classno`,`wangyb_courses`.`wyb_Cname` AS `wyb_Cname`,`wangyb_courses`.`wyb_Chours` AS `wyb_Chours`,`wangyb_courses`.`wyb_Ccredit` AS `wyb_Ccredit`,`wangyb_courses`.`wyb_Ctype` AS `wyb_Ctype` from (`wangyb_courses` join `wangyb_cc`) where (`wangyb_courses`.`wyb_Cno` = `wangyb_cc`.`wyb_Cno`) ;

-- ----------------------------
-- View structure for `teainfo_view`
-- ----------------------------
DROP VIEW IF EXISTS `teainfo_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `teainfo_view` AS select `wangyb_teachers`.`wyb_Tno` AS `wyb_Tno`,`wangyb_teachers`.`wyb_Tname` AS `wyb_Tname`,`wangyb_teachers`.`wyb_Tsex` AS `wyb_Tsex`,`wangyb_teachers`.`wyb_Tage` AS `wyb_Tage`,`wangyb_teachers`.`wyb_Ttitle` AS `wyb_Ttitle`,`wangyb_teachers`.`wyb_Tphone` AS `wyb_Tphone` from `wangyb_teachers` ;

-- ----------------------------
-- Procedure structure for `select_CC`
-- ----------------------------
DROP PROCEDURE IF EXISTS `select_CC`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `select_CC`(in classNo varchar(20))
BEGIN
        SELECT wyb_Cno, wyb_Classno, wyb_Cterm, wyb_Cname, wyb_Tname, wyb_Chours, wyb_Ccredit, wyb_Ctype
        FROM ClassCourse_View where wyb_Classno = classNo;
    END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `select_score`
-- ----------------------------
DROP PROCEDURE IF EXISTS `select_score`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `select_score`()
BEGIN
	SELECT * FROM StuScore_VIEW;
    END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `select_stu_info`
-- ----------------------------
DROP PROCEDURE IF EXISTS `select_stu_info`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `select_stu_info`(in sno varchar(20))
BEGIN
	select * from stuinfo_view where wyb_sno = sno;
    END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `select_TC`
-- ----------------------------
DROP PROCEDURE IF EXISTS `select_TC`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `select_TC`(in id int)
BEGIN
	select * from tc_view where wyb_Cteacher = id;
    END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `select_tea_info`
-- ----------------------------
DROP PROCEDURE IF EXISTS `select_tea_info`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `select_tea_info`(in Tno varchAr(20))
BEGIN
	select * from teainfo_view where wyb_Tno = Tno;
    END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `check_score`;
DELIMITER ;;
CREATE TRIGGER `check_score` AFTER INSERT ON `wangyb_score` FOR EACH ROW BEGIN
	IF new.wyb_score < 0 OR new.wyb_score > 100 THEN
		DELETE FROM wangyb_score WHERE wyb_score = new.wyb_score;
	END IF;		
    END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `Scredit_add`;
DELIMITER ;;
CREATE TRIGGER `Scredit_add` AFTER INSERT ON `wangyb_score` FOR EACH ROW BEGIN
	IF new.wyb_Score >=60 THEN
	  UPDATE wangyb_students S SET wyb_Scredit = wyb_Scredit + (
		SELECT wyb_Ccredit FROM wangyb_courses C WHERE C.wyb_Cno = new.wyb_Cno
	  )WHERE wyb_Sno = new.wyb_Sno;
	end if;
    END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `Scredit_delete`;
DELIMITER ;;
CREATE TRIGGER `Scredit_delete` BEFORE DELETE ON `wangyb_score` FOR EACH ROW BEGIN
	if old.wyb_score >= 60 then
		update wangyb_students S set S.wyb_Scredit = S.wyb_Scredit - (
			select wyb_Ccredit from wangyb_courses C where C.wyb_Cno = old.wyb_Cno 
		) where S.wyb_Sno = old.wyb_Sno;
	end if;
    END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `check_student_age`;
DELIMITER ;;
CREATE TRIGGER `check_student_age` AFTER INSERT ON `wangyb_students` FOR EACH ROW BEGIN
	if new.wyb_Sage < 0 then
		delete from wangyb_students where wyb_Sno = new.wyb_Sno;
	end if;
    END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `check_teacher_age`;
DELIMITER ;;
CREATE TRIGGER `check_teacher_age` AFTER INSERT ON `wangyb_teachers` FOR EACH ROW BEGIN
	IF new.wyb_Tage < 0 THEN
		DELETE FROM wangyb_teachers WHERE wyb_Tno = new.wyb_Tno;
	END IF;
    END
;;
DELIMITER ;
