package com.service.impl;

import com.bean.Choice;
import com.mapper.ChoiceMapper;
import com.service.ChoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChoiceServiceImpl implements ChoiceService {
    @Autowired
    ChoiceMapper choiceMapper;

    @Override
    public List<Choice> queryAllChoiceByTid(String tid) {
        return choiceMapper.queryAllChoiceByTid(tid);
    }

    @Override
    public List<Choice> queryChoiceByCid(String cid) {
        return choiceMapper.queryChoiceByCid(cid);
    }

    @Override
    public int insertChoice(Choice choice) {
        return choiceMapper.addChoice(choice);
    }

    @Override
    public int deleteChoice(String chid) {
        return choiceMapper.deleteChoice(chid);
    }

    @Override
    public List<Choice> queryChoiceByExamId(String examid) {
        return choiceMapper.queryChoiceByExamId(examid);
    }
}
