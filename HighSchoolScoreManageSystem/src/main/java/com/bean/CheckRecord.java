package com.bean;

public class CheckRecord {
    private int Cid;
    private String Tname;
    private String Cname;
    private String Sname;
    private String Sno;
    private String Status;
    private Long CheckTime;
    private String CheckDate;
    private Long EndTime;
    private String isExpire;

    public String getIsExpire() {
        return isExpire;
    }

    public void setIsExpire(String isExpire) {
        this.isExpire = isExpire;
    }

    public Long getEndTime() {
        return EndTime;
    }

    public void setEndTime(Long endTime) {
        EndTime = endTime;
    }

    public String getCheckDate() {
        return CheckDate;
    }

    public void setCheckDate(String checkDate) {
        CheckDate = checkDate;
    }

    public int getCid() {
        return Cid;
    }

    public void setCid(int cid) {
        Cid = cid;
    }

    public String getTname() {
        return Tname;
    }


    public Long getCheckTime() {
        return CheckTime;
    }

    public void setCheckTime(Long checkTime) {
        CheckTime = checkTime;
    }

    public void setTname(String tname) {
        Tname = tname;
    }

    public String getCname() {
        return Cname;
    }

    public void setCname(String cname) {
        Cname = cname;
    }


    public String getSname() {
        return Sname;
    }

    public void setSname(String sname) {
        Sname = sname;
    }

    public String getSno() {
        return Sno;
    }

    public void setSno(String sno) {
        Sno = sno;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }


}
