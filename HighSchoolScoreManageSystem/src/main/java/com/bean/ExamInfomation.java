package com.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExamInfomation {
    //考卷是给1个班级的,多名学生的
    private String id;//考试名称
    private String examname;
    private String password;
    private String chapterId;
    private String chapter;
    private String classId;
    private String teacherId;
    private String teacher;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date begindate;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date enddate;
    private int examtime;
    private int choicenum;
    private int choicescore;
    private int judgenum;
    private int judgescore;
    private int examscore;
}
