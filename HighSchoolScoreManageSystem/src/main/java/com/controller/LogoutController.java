package com.controller;

import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class LogoutController {
    @RequestMapping(value = "/logout")
    //注销
    ModelAndView logout(ModelAndView modelAndView, HttpSession session, HttpServletRequest request){
        session.invalidate();
        String contextPath = request.getContextPath();
        modelAndView.setViewName("redirect:"+contextPath+"/index.jsp");
        return modelAndView;
    }
}
