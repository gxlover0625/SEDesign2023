package com.service.impl;

import com.bean.MyStudent;
import com.bean.Score;
import com.bean.Student;
import com.bean.Teacher;
import com.mapper.StuMapper;
import com.mapper.TeaMapper;
import com.service.TeaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeaServiceImpl implements TeaService {
    @Autowired
    TeaMapper teaMapper;

    @Autowired
    StuMapper stuMapper;
    @Override
    public List<Teacher> getTeaList() {
        return teaMapper.selectTea();
    }

    @Override
    public void addTea(Teacher teacher) {
        //添加教师信息
        teaMapper.insertTea(teacher);
        //设置默认密码
        teaMapper.insertPwd(teacher);
    }

    @Override
    public Teacher getTeaById(String id) {
        return teaMapper.selectTeaById(id);
    }

    @Override
    public List<MyStudent> getMyStuList(String id) {
        return teaMapper.selectMyStu(id);
    }

    @Override
    public void insertScore(String tno, String[] id, String[] term, String[] courseId, String[] score) {
        for (int i = 0; i < id.length; i++) {
            if (score[i] != null && !"".equals(score[i])) {
                Score sc = new Score();
                sc.setID(id[i]);
                sc.setTerm(term[i]);
                sc.setCourseId(courseId[i]);
                sc.setScore(Integer.parseInt(score[i]));
                sc.setTeaId(tno);
                teaMapper.InsertScore(sc);
                //stuMapper.updateScredit(id[i], courseId[i]);
            }
        }
    }

    @Override
    public void removeTea(String id) {
        teaMapper.deleteTea(id);
    }

    @Override
    public void updateTea(Teacher teacher) {
        teaMapper.updateTea(teacher);
    }

    @Override
    public void updatePwd(String id, String newPwd) {
        teaMapper.updatePwd(id, newPwd);
    }

    public void insertCheck(String tno, String Cno, String ClassNo, long beginTime, long endTime, String pwd){
        teaMapper.insertCheck(tno, Cno, ClassNo, beginTime, endTime, pwd);
        return;
    }
}
