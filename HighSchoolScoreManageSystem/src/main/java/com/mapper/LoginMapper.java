package com.mapper;

import com.bean.Admin;
import com.bean.Student;
import com.bean.Teacher;
import org.apache.ibatis.annotations.Param;

public interface LoginMapper {
    Student selectStuPwd(@Param("ID") String id,@Param("password") String password);


    Admin selectAdm(@Param("id") String id,@Param("password") String password);

    Teacher selectTeaPwd(@Param("id") String id,@Param("password") String password);
}
