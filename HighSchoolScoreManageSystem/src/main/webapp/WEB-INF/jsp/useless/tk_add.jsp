<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html;charset=utf-8" isELIgnored="false" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>高校成绩管理系统</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
<%--      <link href="<%=basePath%>resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">--%>
    <link rel="stylesheet" type="text/css" href="<%= basePath%>resources/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="<%=basePath%>resources/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="<%=basePath%>resources/vendor/dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="<%=basePath%>resources/vendor/css/editTable.css" type="text/css" rel="stylesheet"/>
      <script language="javascript" type="text/javascript" src="<%=basePath%>resources/vendor/js/jquery-1.7.2.min.js"></script>
      <script language="javascript" type="text/javascript" src="<%=basePath%>resources/vendor/js/editTable.js"></script>
  </head>
  <style>
      .line {
          width: 70px;
          overflow: hidden;
          text-align: left;
          text-overflow: ellipsis;
          white-space: nowrap;
      }

      textarea:focus {
          border-color: #719ECE;
          box-shadow: 0 0 10px #719ECE;
      }
  </style>
  <script type="text/javascript">
      $(function(){
          //实现全选与反选 var ids=[];
          $("#allAndNotAll").click(function() {
              if (this.checked){
                  $("input[name='items']:checkbox").each(function(){
                      $(this).attr("checked", true);
                  });
              } else {
                  $("input[name='items']:checkbox").each(function() {
                      $(this).attr("checked", false);
                  });
              }
          });
          //获取被选中的id

          $('#getAllSelectedId').click(function(){
              var ids=new Array();
              $("input[name='items']:checked").each(function(){
                  ids.push($(this).attr("id"));
              });
              var chk_value=ids.join(",");
              location.href = "<%=basePath%>/tk/deletemanyjudge.do?chk_value="+chk_value;
              alert("获取的id"+chk_value);
          });
      });
  </script>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="<%=path%>/admin/info">教务管理系统</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="<%=basePath%>logout"><i class="fa fa-sign-out fa-lg"></i> 退出</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
        <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" style="width: 80px; height: 80px;" src="<%=basePath%>resources/img/user.png" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">${teacher.name}</p>
          <p class="app-sidebar__user-designation">教师</p>
        </div>
      </div>
      <ul class="app-menu">
          <li><a class="app-menu__item " href="<%=path%>/teacher/index"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">个人信息</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/courses"><i class="app-menu__icon fa fa fa-book"></i><span class="app-menu__label">我的任课</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/myStudent"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">我的学生</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/score"><i class="app-menu__icon fa fa-align-left"></i><span class="app-menu__label">学生成绩</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/scoreInsert"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">成绩录入</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/check"><i class="app-menu__icon fa fa-check"></i><span class="app-menu__label">发起签到</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/kpoints"><i class="app-menu__icon fa fa-bookmark-o"></i><span class="app-menu__label">知识点</span></a></li>
          <li><a class="app-menu__item active" href="<%=path%>/teacher/tk"><i class="app-menu__icon fa fa-list-alt"></i><span class="app-menu__label">题库</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/updatePwd"><i class="app-menu__icon fa fa-certificate"></i><span class="app-menu__label">修改密码</span></a></li>
      </ul>
    </aside>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-book"></i>题库</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="<%=path%>/teacher/tk">题库</a></li>
        </ul>
      </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <table class="table table-hover table-bordered" id="tktable">
                        <thead>
                        <tr>
                            <th colspan="6" style="text-align:left ;font-size: 20px"><button class="line btn btn-primary btn-sm" onclick="add()" >添加新题</button>&nbsp;&nbsp;&nbsp;
                                <button class="line btn btn-primary btn-sm" id="getAllSelectedId">批量删除</button></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td ><input type="checkbox" id="allAndNotAll" />全选/反选</td>
                            <td>题号</td>
                            <td>题目</td>
                            <td>正确答案</td>
                            <td>难度</td>
                            <td class="del-col">操作</td>
                        </tr>
                        <c:forEach items="${pageInfo.list}" var="ch">
                            <tr>
                                <td ><input type="checkbox" name=items id="${ch.id}"/></td>
                                <c:set var="index" value="${index+1}"/>
                                <td>${index}</td>
                                <td class="line">${ch.content}</td>
                                <td>${ch.answer}</td>
                                <td>${ch.difficulty}</td>
                                <td class="del-col">
                                    <a href="#" onclick="return edit(${ch.id})" style="text-decoration: none;">
                                        <span class="fa fa-edit fa-fw"></span>
                                    </a>
                                    <a href="#" onclick="return trash(${ch.id})" style="text-decoration: none;"
                                       data-toggle="modal" data-target="#trashModal">
                                        <span class="fa fa-trash-o fa-fw"></span>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <div class="row">
                        <!--分页文字信息  -->
                        <div class="col-md-6">当前 ${pageInfo.pageNum }页,总${pageInfo.pages }
                            页,总 ${pageInfo.total } 道题目</div>
                        <!-- 分页条信息 -->
                        <div class="col-md-6">
                            <nav aria-label="Page navigation">
                                <ul class="pagination">
                                    <li><a href="<%=basePath%>/teacher/tk?pn=1">首页</a></li>
                                    <c:if test="${pageInfo.hasPreviousPage }">
                                        <li><a href="<%=basePath%>/teacher/tk?pn=${pageInfo.pageNum-1}"
                                               aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
                                        </a></li>
                                    </c:if>

                                    <c:forEach items="${pageInfo.navigatepageNums }" var="page_Num">
                                        <c:if test="${page_Num == pageInfo.pageNum }">
                                            <li class="active"><a href="#">${page_Num }</a></li>
                                        </c:if>
                                        <c:if test="${page_Num != pageInfo.pageNum }">
                                            <li><a href="<%=basePath%>/teacher/tk?pn=${page_Num }">${page_Num }</a></li>
                                        </c:if>

                                    </c:forEach>
                                    <c:if test="${pageInfo.hasNextPage }">
                                        <li><a href="<%=basePath%>/teacher/tk?pn=${pageInfo.pageNum+1 }"
                                               aria-label="Next"> <span aria-hidden="true">&raquo;</span>
                                        </a></li>
                                    </c:if>
                                    <li><a href="<%=basePath%>/teacher/tk?pn=${pageInfo.pages}">末页</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="modal fade" id="editch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myedit">编辑</h4>
                                </div>
                                <form class="form-horizontal" action="<%=basePath%>/tk/upjudge.do" method="post">
                                    <div class="modal-body">
                                        <input name="id" id="id" hidden="hidden"/>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon" style="height: 40px ; tab-size: 16px ">题目：</span>
                                            <textarea class="form-control" rows="3" name="content" id="upcontent"></textarea>
                                        </div>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon" style="height: 40px ; tab-size: 16px ">答案：</span>
                                            <div class="col-sm-5">
                                                <select class="form-control" id="upanswer" name="answer">
                                                    <option value="true">正确</option>
                                                    <option value="false">错误</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon" style="height: 40px ; tab-size: 16px ">分析：</span>
                                            <textarea class="form-control" rows="3" name="analysis" id="upanalysis"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">知识点</label>
                                            <div class="col-sm-6">
                                                <select class="form-control" id="upchapter" name="chapter">
                                                </select>
                                                <select class="form-control" id="upchaptertwo" name="chaptertwo"></select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">难度</label>
                                            <div class="col-sm-5">
                                                <select class="form-control" id="updifficulty" name = "difficulty">
                                                    <option value="1">容易</option>
                                                    <option value="2">中等</option>
                                                    <option value="3">较难</option>
                                                    <option value="4">困难</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                                        <button type="submit" class="btn btn-primary" id="upaddbut">提交</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    </td>
                    </tr>
                    <!-- Button trigger modal -->
                    <!-- /.modal -->
                    <!-- Modal添加的模态框 -->
                    <div class="modal fade" id="addch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <b >添加题目</b>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <form class="form-horizontal" action="<%=basePath%>/teacher/createjudge" method="post">
                                    <div class="modal-body">
                                        <%--<input name="id" id="id" hidden="hidden"/>--%>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon" style="height: 40px ; tab-size: 16px ">题目：</span>
                                            <textarea class="form-control" rows="3" name="content" id="content"></textarea>
                                        </div>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon" style="height: 40px ; tab-size: 16px ">答案：</span>
                                            <div class="col-sm-3">
                                                <select class="form-control" id="answer" name="answer">
                                                    <option value="true">正确</option>
                                                    <option value="false">错误</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon" style="height: 40px ; tab-size: 16px ">分析：</span>
                                            <textarea class="form-control" rows="3" name="analysis" id="analysis"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">知识点:</label>
                                            <div class="col-sm-4">
                                                <select class="form-control offset-2" id="chapter" name="chapter">
                                                </select>
                                                <select class="form-control offset-2" id="chaptertwo" name="chaptertwo"></select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">难度:</label>
                                            <div class="col-sm-4">
                                                <select class="form-control offset-2" id="difficulty" name = "difficulty">
                                                    <option value="1">容易</option>
                                                    <option value="2">中等</option>
                                                    <option value="3">较难</option>
                                                    <option value="4">困难</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                                        <button type="submit" class="btn btn-primary" id="addbut">提交</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    </td>
                    </tr>
                    <!--删除的模态框-->
                    <div class="modal fade" id="trashModal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <!-- 模糊框头部 -->
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                    </button>
                                    <h4 class="modal-title">删除！</h4>
                                </div>
                                <!-- 模糊框主体 -->
                                <div class="modal-body">
                                    <strong>你确定要删除吗？</strong>
                                </div>
                                <!-- 模糊框底部 -->
                                <div class="modal-footer">
                                    <button type="button" class="delSure btn btn-info" data-dismiss="modal">确定</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body">
                        <table class="table table-hover table-bordered" id="coursesTable">
                            <thead>
                            <tr>
                                <th>学期</th>
                                <th>班级名称</th>
                                <th>课程名称</th>
                                <th>学时</th>
                                <th>学分</th>
                                <th>考查类型</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${teaCourseList}" var="course" varStatus="">
                                <tr>
                                    <td>${course.term}</td>
                                    <td>${course.className}</td>
                                    <td>${course.name}</td>
                                    <td>${course.hours}</td>
                                    <td>${course.credit}</td>
                                    <td>${course.type}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="<%= basePath%>resources/js/jquery-3.2.1.min.js"></script>
    <script src="<%= basePath%>resources/js/popper.min.js"></script>
    <script src="<%= basePath%>resources/js/bootstrap.min.js"></script>
    <script src="<%= basePath%>resources/js/main.js"></script>
    <!-- Data table plugin-->
    <!-- The javascript plugin to display page loading on top-->
    <script src="<%= basePath%>resources/js/plugins/pace.min.js"></script>
    <script type="text/javascript" src="<%= basePath%>resources/js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<%= basePath%>resources/js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">$('#testTable').DataTable();</script>

    <script>
        function add() {
            getpoint();
            $("#addch").modal({
                backdrop: "static"
            });
        };
        function getpoint() {
            $("#chapter").empty();
            $("#chaptertwo").empty();
            $.ajax({
                url: "${APP_PATH}/teacher/allpoint1",
                type: "GET",
                success: function (data) {

                    var chapter = $(document).find("#chapter");
                    var chaptertwo = $(document).find("#chaptertwo");
                    // for (var i = 0; i < data.length; i++) {
                    //     chapter.append("<option value='" + data[i].id + "'>" + data[i].pointname + "</option>");
                    // }
                    // for (var j = 0; j < data[0].point2.length; j++) {
                    //     console.log("当前位置" + data[0].point2[j].pname + "内容为" + data[0].point2[j].pname);
                    //     chaptertwo.append("<option value='" + data[0].point2[j].pname + "'>" + data[0].point2[j].pname + "</option>");
                    // }
                    var  num= data.length;
                    var pp = new Array(num+1);
                    for (var i=0;i<data.length;i++){
                        chapter.append("<option value='" + i + "'>" + data[i].pointname + "</option>");
                    }
                    for(var j=0;j<data[0].point2.length;j++){
                        chaptertwo.append("<option value='" + data[0].point2[j].bid + "'>" + data[0].point2[j].pname + "</option>");
                    }
                    //select1绑定change事件
                    $("#chapter").change(function () {
                        var p = this.value;
                        //p = p-1;
                        // console.log("第几个"+p);
                        $("#chaptertwo").empty();
                        //遍历p2的数组
                        // console.log("长度为"+ data[p].point2.length);
                        for (var j = 0; j < data[p].point2.length; j++) {
                            console.log(data[p].point2[j].bid,data[p].point2[j].pname);
                            chaptertwo.append("<option value='" + data[p].point2[j].bid+ "'>" + data[p].point2[j].pname+ "</option>");
                        }
                    });
                }
            });
        }

        // 编辑信息的方法
        function edit(id) {
            // 先去查询数据
            $.ajax({
                url: '<%=basePath%>/tk/judgeId.do',
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json;charset=UTF-8',
                data: JSON.stringify({
                    id: id
                }),
                success: function (data) {
                    getpt(data.chapter);
                    $("#id").val(data.id);
                    $("#upcontent").val(data.content);
                    $("#upanswer").val(data.answer);
                    $("#upanalysis").val(data.analysis);
                    $("#upchapter").val(data.chapter);
                    console.log("题目的chapter" + data.chapter);
                    console.log("select当前的value" +   $("#upchapter").val());
                    console.log(data.chapter);
                    $("#upchaptertwo").val(data.chaptertwo);
                    console.log(data.chaptertwo);
                    $("#updifficulty").val(data.difficulty);
                    $("#editch").modal('show');
                },
                error: function () {
                    alert("错误");
                }
            });
        }
        function getpt(chid) {
            console.log("当前复选框" + chid);
            $("#upchapter").empty();
            $("#upchaptertwo").empty();
            $.ajax({
                url:"<%=basePath%>/teacher/allpoint1",
                type:"GET",
                success:function (data){
                    alert('1');
                    console.log(data);
                    var upchapter = $(document).find("#upchapter");
                    var upchaptertwo = $(document).find("#upchaptertwo");
                    for(var i = 0; i < data.length; i++){
                        console.log("当前select的option="+ data[i].id);
                        upchapter.append("<option value='" + data[i].id + "'>" + data[i].pointname + "</option>");
                        console.log("当前chapter的value" + data[i].id);
                    }for(var j = 0; j < data[chid-1].point2.length; j++) {
                        upchaptertwo.append("<option value='" + data[chid-1].point2[j].pname + "'>" + data[chid-1].point2[j].pname + "</option>");
                    }
                    //select1绑定change事件
                    $("#upchapter").change(function () {
                        var p = this.value;
                        p = p-1;
                        //删除原来的信息
                        $("#upchaptertwo").empty();
                        for (var j = 0; j < data[p].point2.length; j++) {
                            upchaptertwo.append("<option value='" + data[p].point2[j].pname+ "'>" + data[p].point2[j].pname+ "</option>");
                        }
                    });
                }
            })
        }
        //删除
        function trash(id) {
            if (!id) {
                alert("error");
            } else {
                $(".delSure").click(function () {
                    $.ajax({
                        url: '<%=basePath%>/tk/deletejudge.do?id=' + id,
                        type: 'POST',
                        success: function (data) {
                            $("body").html(data);
                        }
                    });
                });
            }
        }
    </script>
  </body>
</html>