package com.service;

import com.bean.ExamInfomation;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ExamService {
    List<ExamInfomation> queryExamByTid(String tid);

    int addExam(ExamInfomation examInfomation);

    int queryExamId();

    ExamInfomation queryExamInfoByExamId(@Param("examid")String examid);
}
