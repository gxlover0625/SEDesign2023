package com.service.impl;

import com.bean.Course;
import com.bean.StuCourse;
import com.bean.TeaCourse;
import com.mapper.CouMapper;
import com.service.CouService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CouServiceImpl implements CouService {
    @Autowired
    CouMapper couMapper;
    @Override
    public List<Course> getCourseList() {
        return couMapper.selectCouList();
    }

    @Override
    public void addCourse(Course course) {
        couMapper.insertCou(course);
    }

    @Override
    public List<Course> getClassCouList() {
        return couMapper.selectClassCouList();
    }

    @Override
    public void addClassCourse(String classNo, String ID) {
        couMapper.insertClassCou(classNo, ID);
    }

    @Override
    public List<Course> getStuCourse(String id) {
        return couMapper.selectStuCouList(id);
    }

    @Override
    public List<Course> getCouListByClass(String className) {
        return couMapper.selectCouByClass(className);
    }

    @Override
    public void chooseCou(String id, String courseId) {
        couMapper.insertSC(id, courseId);
    }

    @Override
    public void dropCou(String id, String courseId) {
        couMapper.deleteSC(id, courseId);
    }

    @Override
    public List<TeaCourse> getTeaCouList(String id) {
        return couMapper.selectTeaCouList(id);
    }

    @Override
    public void removeCou(String id) {
        couMapper.deteleCou(id);
    }

    @Override
    public Course getCouById(String id) {
        return couMapper.selectCouById(id);
    }

    @Override
    public void updateCou(Course course) {
        couMapper.updateCou(course);
    }

    @Override
    public void removeClassCou(String classNo, String id) {
        couMapper.deleteClassCou(classNo, id);
    }

    @Override
    public List<String> selectClassidByTid(String tid) {
        return couMapper.selectClassidByTid(tid);
    }

}
