<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html;charset=utf-8" isELIgnored="false" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>高校成绩管理系统</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="<%= basePath%>resources/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="<%=path%>/teacher/index">教务管理系统</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="<%=basePath%>logout"><i class="fa fa-sign-out fa-lg"></i> 退出</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" style="width: 80px; height: 80px;" src="<%=basePath%>resources/img/user.png" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">${sessionScope.teacher.name}</p>
          <p class="app-sidebar__user-designation">教师</p>
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item" href="<%=path%>/teacher/index"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">个人信息</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/courses"><i class="app-menu__icon fa fa-book"></i><span class="app-menu__label">我的任课</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/myStudent"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">我的学生</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/score"><i class="app-menu__icon fa fa-align-left"></i><span class="app-menu__label">学生成绩</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/scoreInsert"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">成绩录入</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/check"><i class="app-menu__icon fa fa-check"></i><span class="app-menu__label">发起签到</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/kpoints"><i class="app-menu__icon fa fa-bookmark-o"></i><span class="app-menu__label">知识点</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/tk"><i class="app-menu__icon fa fa-list-alt"></i><span class="app-menu__label">题库-判断题</span></a></li>
        <li><a class="app-menu__item active" href="<%=path%>/teacher/tkchoice"><i class="app-menu__icon fa fa-list-alt"></i><span class="app-menu__label">题库-选择题</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/examinfo"><i class="app-menu__icon fa fa-history"></i><span class="app-menu__label">考试历史</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/teacher/updatePwd"><i class="app-menu__icon fa fa-certificate"></i><span class="app-menu__label">修改密码</span></a></li>

      </ul>
    </aside>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-list-alt"></i> 题库-选择题</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-list-alt"></i></li>
          <li class="breadcrumb-item active"><a href="<%=path%>/teacher/tkchoice">题库-选择题</a></li>
        </ul>
      </div>

      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <form class="form-group col-md-12" action="<%=path%>/teacher/addchoice" method="post">
              <div class="form-group">

                <div class="row">
                    <div class="col-lg-auto">
                        <label>题目名称</label>
                        <textarea class="form-control" rows="2" cols="40" name="content"></textarea>
                    </div>
                    <div class="col-lg-auto">
                        <label>题目解析</label>
                        <textarea class="form-control" rows="2" cols="40" name="analysis"></textarea>
                    </div>
                </div>

                  <div class="row">
                      <div class="col-lg-auto">
                          <label>A</label>
                          <textarea class="form-control" rows="2" cols="30" name="aoption"></textarea>
                      </div>
                      <div class="col-lg-auto">
                          <label>B</label>
                          <textarea class="form-control" rows="2" cols="30" name="boption"></textarea>
                      </div>
                      <div class="col-lg-auto">
                          <label>C</label>
                          <textarea class="form-control" rows="2" cols="30" name="coption"></textarea>
                      </div>
                      <div class="col-lg-auto">
                          <label>D</label>
                          <textarea class="form-control" rows="2" cols="30" name="doption"></textarea>
                      </div>
                  </div>
              </div>

                <div class="row">
                    <div class="col-lg-3">
                        <label>章节名</label>
                        <select class="form-control" name="chapterId" required id="point1select">
                            <%--                      <option value="" disabled selected>请选择课程</option>--%>
                            <c:forEach items="${point1list}" var="point1" varStatus="">
                                <option value="${point1.id}">
                                        ${point1.pointname}
                                </option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="col-lg-3">
                        <label>小节名</label>
                        <select class="form-control" name="chaptertwoId" required id="point2select">
                            <%--                      <option value="" disabled selected>请选择课程</option>--%>
                            <c:forEach items="${point2list}" var="point2" varStatus="">
                                <option value="${point2.bid}">
                                        ${point2.pname}
                                </option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="col-lg-3">
                        <label>正确答案</label>
                        <select class="form-control" name="answer" required id="ansselect">
                            <%--                      <option value="" disabled selected>请选择课程</option>--%>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                        </select>
                    </div>

                    <div class="col-lg-3">
                        <label>难度</label>
                        <select class="form-control" name="difficulty" required id="difficultyselect">
                            <%--                      <option value="" disabled selected>请选择课程</option>--%>
                            <option value="1">容易</option>
                            <option value="2">中等</option>
                            <option value="3">较难</option>
                            <option value="4">困难</option>
                        </select>
                    </div>
                </div>

              <div class="tile-footer"><button class="btn btn-primary" type="submit" >添加题目</button></div>
            </form>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <table class="table table-hover table-bordered" id="coursesTable">
                <thead>
                  <tr>
                    <th>题号</th>
                    <th>题目</th>
                    <th>A选项</th>
                    <th>B选项</th>
                    <th>C选项</th>
                    <th>D选项</th>
                    <th>章节</th>
                    <th>小节</th>
                    <th>难度</th>
                      <th class="del-col">操作</th>
                  </tr>
                </thead>
                <tbody>
                    <c:forEach items="${choices}" var="choice" varStatus="">
                        <tr>
                            <c:set var="index" value="${index+1}"/>
                            <td>${index}</td>
                            <td>${choice.content}</td>
                            <td>${choice.aoption}</td>
                            <td>${choice.boption}</td>
                            <td>${choice.coption}</td>
                            <td>${choice.doption}</td>
                            <td>${choice.chapter}</td>
                            <td>${choice.chaptertwo}</td>
                            <td>${choice.difficulty}</td>
                            <td class="del-col">
                                    <%--<a href="#" onclick="return edit(${judge.id})" style="text-decoration: none;">
                                      <span class="fa fa-edit fa-fw"></span>
                                    </a>--%>
                                <a href="<%=path%>/teacher/deleteChoice?chid=${choice.id}" onclick="return confirm('你确定要删除嘛?')" style="text-decoration: none;">
                                    <span class="fa fa-trash-o fa-fw"></span>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="<%= basePath%>resources/js/jquery-3.2.1.min.js"></script>
    <script src="<%= basePath%>resources/js/popper.min.js"></script>
    <script src="<%= basePath%>resources/js/bootstrap.min.js"></script>
    <script src="<%= basePath%>resources/js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="<%= basePath%>resources/js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
    <!-- Data table plugin-->
    <script type="text/javascript" src="<%= basePath%>resources/js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<%= basePath%>resources/js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">$('#coursesTable').DataTable();</script>

    <script>
      $("#point1select").change(function () {
          var p=this.value;
        $.ajax({
          url: '<%=basePath%>/teacher/querypoint2?id='+p,
          type:'POST',
            success:function (data) {
                $("#point2select").empty();
                var len=data.length;
                for(var i=0;i<len;i++){
                    $("#point2select").append("<option value='"+data[i].bid+"'>"+data[i].pname+"</option>")
                }
            },
            error:function () {
                alert('错误');
            }
        })
      })
    </script>
  </body>
</html>