package com.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Choice {
    private Long id;
    private String content;
    private String aoption;
    private String boption;
    private String coption;
    private String doption;
    private String answer;
    private String analysis;
    private String chapterId;
    private String chapter;
    private String chaptertwo;
    private String chaptertwoId;
    private String difficulty;
}
