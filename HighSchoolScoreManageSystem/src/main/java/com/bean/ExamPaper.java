package com.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExamPaper {
    private ExamInfomation examInfomation;
    List<Judge> paperJudgeList;
    List<Choice> paperChoiceList;
}
