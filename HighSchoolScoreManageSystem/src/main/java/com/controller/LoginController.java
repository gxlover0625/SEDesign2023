package com.controller;

import com.bean.Admin;
import com.bean.Student;
import com.bean.Teacher;
import com.service.LoginService;
import com.service.StuService;
import com.service.TeaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

@Controller
public class LoginController {
    @Autowired
    LoginService loginService;

    @Autowired
    StuService stuService;

    @Autowired
    TeaService teaService;

    @RequestMapping(value = "/login",
    method = RequestMethod.POST
    )
    ModelAndView login(ModelAndView modelAndView, HttpSession session, HttpServletRequest request, HttpServletResponse response, String type, String ID, String password) throws IOException, ServletException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        Boolean flag = false;
        if("admin".equals(type)){
            Admin admin = loginService.checkAdmPassword(ID, password);
            if(admin != null){
                flag = true;
                session.setAttribute("admin", admin);
                modelAndView.setViewName("admin/index");
            }
        }
        if("teacher".equals(type)){
            Teacher teacher = loginService.checkTeaPassword(ID, password);
            if(teacher != null){
                flag = true;
                teacher = teaService.getTeaById(ID);
                session.setAttribute("teacher", teacher);
                //modelAndView.setViewName("teacher/index");
                modelAndView.setViewName("redirect:" + contextPath +"/teacher/index");
            }
        }
        if("student".equals(type)){
            Student student = loginService.checkStuPassword(ID, password);
            if(student != null){

                flag = true;
                student = stuService.getStuById(ID);
                session.setAttribute("student", student);
                //modelAndView.setViewName("student/index");
                modelAndView.setViewName("redirect:" + contextPath +"/student/index");
            }
        }
        if(!flag){
            session.setAttribute("message", "用户名或密码错误！");
            modelAndView.setViewName("redirect:" + contextPath +"/index.jsp");
        }
        return modelAndView;

    }
}
