package com.service.impl;

import com.bean.Article;
import com.mapper.ArticleMapper;
import com.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    ArticleMapper articleMapper;
    @Override
    public void insertArticle(String id, String title, String content, String departmentID, String courseID, String teacherID, String pubDate) {
        articleMapper.insertArticle(id, title, content, departmentID, courseID, teacherID, pubDate);
    }

    @Override
    public List<Article> getArticleList() {
        return articleMapper.selectArticleList();
    }

    @Override
    public List<Article> getArticleListBySno(String id) {
        return articleMapper.selectArticleListBySno(id);
    }

    @Override
    public void deleteArticleListByAid(String id) {
        articleMapper.deleteArticleById(id);
    }

    @Override
    public Article getArticleByID(String id) {
        return articleMapper.selectArticleById(id);
    }

    @Override
    public void updateArticleById(Article article) {
        articleMapper.updateArticle(article);
    }

    @Override
    public List<Article> getArticleListByKeywords(String keywords, String departmenID, String courseID, String teacherID) {
        return articleMapper.selectArticleListByKeywords(keywords, departmenID, courseID, teacherID);
    }
}
