<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html;charset=utf-8" isELIgnored="false" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>高校成绩管理系统</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="<%= basePath%>resources/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <style type="text/css">
        form .el{
            float: left;
            margin-left: 20px;
        }
    </style>
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="<%=path%>/student/index">教务管理系统</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="<%=basePath%>logout"><i class="fa fa-sign-out fa-lg"></i> 退出</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
        <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" style="width: 80px; height: 80px;" src="<%=basePath%>resources/img/user.png" alt="User Image">


            <div>
          <p class="app-sidebar__user-name">${student.name}</p>
          <p class="app-sidebar__user-designation">学生</p>
        </div>
      </div>
      <ul class="app-menu">
          <li><a class="app-menu__item" href="<%=path%>/student/index"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">个人信息</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/student/courses"><i class="app-menu__icon fa fa fa-book"></i><span class="app-menu__label">我的课程</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/student/score"><i class="app-menu__icon fa fa-align-left"></i><span class="app-menu__label">成绩查询</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/student/classCourse"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">选课</span></a></li>
          <li><a class="app-menu__item  active" href="<%=path%>/student/check"><i class="app-menu__icon fa fa-bell"></i><span class="app-menu__label">课程签到</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/student/article"><i class="app-menu__icon fa fa-comments"></i><span class="app-menu__label">学生论坛</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/student/myArticle"><i class="app-menu__icon fa fa-comment"></i><span class="app-menu__label">我的发言</span></a></li>
          <li><a class="app-menu__item" href="<%=path%>/student/updatePwd?reqForward=true"><i class="app-menu__icon fa fa-certificate"></i><span class="app-menu__label">修改密码</span></a></li>

      </ul>
    </aside>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa fa-edit"></i> 课程签到</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="<%=path%>/student/classCourse">课程签到</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">

              <input type="hidden" id="courseId" name="courseId" />
              <table class="table table-hover table-bordered" id="coursesTable">
                <thead>
                  <tr>
                    <th>教师</th>
                    <th>课程</th>
                    <th>签到状态</th>
                    <th>签到时间</th>

                    <th>操作</th>
                  </tr>
                </thead>
                <tbody>
                    <c:forEach items="${stuRecordList}" var="rd" varStatus="status">
                        <tr>
                            <td>${rd.tname}</td>
                            <td>${rd.cname}</td>
                            <td>
                                <c:choose>
                                    <c:when test="${rd.status == '0'}">
                                        未签
                                    </c:when>
                                    <c:when test="${rd.status == '1'}">
                                        已签
                                    </c:when>

                                </c:choose>
                            </td>

                            <td>${rd.checkDate}</td>

                            <td>
                                <c:choose>
                                    <c:when test="${rd.isExpire == '1'}">
                                        已过期
                                    </c:when>
                                    <c:when test="${rd.status == '0'&&rd.isExpire=='0'}">
                                        <form class="tile-body" id="checkForm" method="post" action="<%=path%>/student/updateRecord?Cid=${rd.cid}">
                                            <input class="form-control el"  type="text" style="width: 400px" name="pwd" placeholder="请输入签到码" required>
                                            <button class="btn btn-primary el"  type="submit" >签到</button>
                                        </form>
                                    </c:when>
                                </c:choose>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
              </table>

          </div>
        </div>
      </div>
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="<%= basePath%>resources/js/jquery-3.2.1.min.js"></script>
    <script src="<%= basePath%>resources/js/popper.min.js"></script>
    <script src="<%= basePath%>resources/js/bootstrap.min.js"></script>
    <script src="<%= basePath%>resources/js/main.js"></script>
    <!-- Data table plugin-->
    <script type="text/javascript" src="<%= basePath%>resources/js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<%= basePath%>resources/js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">$('#coursesTable').DataTable();</script>
    <script type="text/javascript">
        function check(basePath, cid) {
            var id = document.getElementById("cid");
            id.value = courseId;
            var chooseCourseForm = document.getElementById("checkForm");
            chooseCourseForm.setAttribute("action", basePath + "student/chooseCourse");
            chooseCourseForm.submit();
        }

    </script>
  </body>
</html>