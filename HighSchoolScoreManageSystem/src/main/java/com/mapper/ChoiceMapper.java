package com.mapper;

import com.bean.Choice;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

import java.util.List;

public interface ChoiceMapper {
    List<Choice> queryAllChoiceByTid(@Param("tid")String tid);

    int addChoice(Choice choice);

    int deleteChoice(@Param("chid")String chid);

    List<Choice> queryChoiceByCid(@Param("cid")String cid);

    List<Choice> queryChoiceByExamId(@Param("examid")String examid);
}
