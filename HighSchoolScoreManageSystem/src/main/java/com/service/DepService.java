package com.service;

import com.bean.Department;

import java.util.List;

public interface DepService {
    List<Department> getDepInfo();

    void addDep(String id, String name);

    void removeDep(String id);

    void updateDep(String id, String name);
}
