package com.service;

import com.bean.Article;
import org.springframework.stereotype.Service;

import java.util.List;


public interface ArticleService {
    void insertArticle(String id, String title, String content, String departmentID, String courseID, String teacherID, String pubDate);

    List<Article> getArticleList();

    List<Article> getArticleListBySno(String id);

    void deleteArticleListByAid(String id);

    Article getArticleByID(String id);

    void updateArticleById(Article article);

    List<Article> getArticleListByKeywords(String keywords, String departmenID, String courseID, String teacherID);
}
