package com.service.impl;

import com.bean.Choice;
import com.bean.ExamPaper;
import com.bean.Judge;
import com.mapper.ExamPaperMapper;
import com.service.ExamPaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExamPaperServiceImpl implements ExamPaperService {
    @Autowired
    ExamPaperMapper examPaperMapper;

    @Override
    public ExamPaper queryExamPaperById(String id) {
        return examPaperMapper.queryExamPaperById(id);
    }

    @Override
    public List<Judge> queryJudgeListByExamId(String examid) {
        return examPaperMapper.queryJudgeListByExamId(examid);
    }

    @Override
    public List<Choice> queryChoiceListByExamId(String examid) {
        return examPaperMapper.queryChoiceListByExamId(examid);
    }

    @Override
    public int addExamPaperChoice(String examid, String choiceid) {
        return examPaperMapper.addExamPaperChoice(examid,choiceid);
    }

    @Override
    public int addExamPaperJudge(String examid, String judgeid) {
        return examPaperMapper.addExamPaperJudge(examid, judgeid);
    }
}
