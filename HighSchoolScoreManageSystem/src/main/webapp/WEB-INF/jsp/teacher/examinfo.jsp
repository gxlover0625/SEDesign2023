<%@ page import="java.util.Date" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html;charset=utf-8" isELIgnored="false" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>高校成绩管理系统</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="<%= basePath%>resources/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="<%=path%>/teacher/index">教务管理系统</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="<%=basePath%>logout"><i class="fa fa-sign-out fa-lg"></i> 退出</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" style="width: 80px; height: 80px;" src="<%=basePath%>resources/img/user.png" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">${sessionScope.teacher.name}</p>
          <p class="app-sidebar__user-designation">教师</p>
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item" href="<%=path%>/teacher/index"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">个人信息</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/courses"><i class="app-menu__icon fa fa-book"></i><span class="app-menu__label">我的任课</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/myStudent"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">我的学生</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/score"><i class="app-menu__icon fa fa-align-left"></i><span class="app-menu__label">学生成绩</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/scoreInsert"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">成绩录入</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/check"><i class="app-menu__icon fa fa-check"></i><span class="app-menu__label">发起签到</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/kpoints"><i class="app-menu__icon fa fa-bookmark-o"></i><span class="app-menu__label">知识点</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/tk"><i class="app-menu__icon fa fa-list-alt"></i><span class="app-menu__label">题库-判断题</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/tkchoice"><i class="app-menu__icon fa fa-list-alt"></i><span class="app-menu__label">题库-选择题</span></a></li>
        <li><a class="app-menu__item active" href="<%=path%>/teacher/examinfo"><i class="app-menu__icon fa fa-history"></i><span class="app-menu__label">考试历史</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/updatePwd"><i class="app-menu__icon fa fa-certificate"></i><span class="app-menu__label">修改密码</span></a></li>

      </ul>
    </aside>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-history"></i> 考试历史</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><i class="fa fa-history"></i></li>
          <li class="breadcrumb-item active"><a href="<%=path%>/teacher/examinfo">考试历史</a></li>
        </ul>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <form class="form-group col-md-12" action="<%=path%>/teacher/addExam" method="post">
              <div class="row">

                <div class="col-lg-auto">
                  <label>考试名称</label>
                  <input class="form-control" type="text" name="examname" placeholder="请输入考试名称" required>
                </div>

                <div class="col-lg-auto">
                  <label>考试密码</label>
                  <input class="form-control" type="text" name="password" placeholder="请输入考试密码" required>
                </div>

                <div class="col-lg-3">
                  <label>班级</label>
                  <select class="form-control" name="classid" required>
                    <option value="" disabled selected>请选择班级</option>
                    <c:forEach items="${ClassidList}" var="classid" varStatus="">
                      <option value="${classid}">
                          ${classid}
                      </option>
                    </c:forEach>
                  </select>
                </div>

                <div class="col-lg-3">
                  <label>章节</label>
                  <select class="form-control" name="aid" required>
                    <option value="" disabled selected>请选择章节</option>
                    <c:forEach items="${pointselects}" var="point1" varStatus="">
                      <option value="${point1.id}">
                          ${point1.pointname}
                      </option>
                    </c:forEach>
                  </select>
                </div>
              </div>

                <div class="row">
                  <div class="col-lg-auto">
                    <label>考试开始时间</label>
                    <input class="form-control" type="datetime-local" name="begindate" placeholder="请输入考试开始时间" required>
                  </div>
                  <div class="col-lg-auto">
                    <label>考试结束时间</label>
                    <input class="form-control" type="datetime-local" name="enddate" placeholder="请输入考试结束时间" required>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-auto">
                    <label>判断题个数</label>
                    <input class="form-control" type="text" name="choicenum" placeholder="请输入判断题个数" required>
                  </div>
                  <div class="col-lg-auto">
                    <label>判断题每题分数</label>
                    <input class="form-control" type="text" name="choicescore" placeholder="请输入判断题每题分数" required>
                  </div>
                  <div class="col-lg-auto">
                    <label>选择题个数</label>
                    <input class="form-control" type="text" name="judgenum" placeholder="请输入选择题个数" required>
                  </div>
                  <div class="col-lg-auto">
                    <label>选择题每题分数</label>
                    <input class="form-control" type="text" name="judgescore" placeholder="请输入选择题每题分数" required>
                  </div>
                </div>
              <div class="tile-footer"><button class="btn btn-primary" type="submit" >添加试卷</button></div>
            </form>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <table class="table table-hover table-bordered" id="coursesTable">
                <thead>
                  <tr>
                    <th>考试序号</th>
                    <th>考试名称</th>
                    <th>考试密码</th>
                    <th>章节</th>
                    <th>开始日期</th>
                    <th>结束日期</th>
                    <th>班级</th>
                    <th>考试时间/分</th>
                    <th>考试分数</th>
                    <th>状态</th>
                    <th>操作</th>
                  </tr>
                </thead>
                <tbody>
                    <c:forEach items="${exmalist}" var="exam" varStatus="">
                        <tr>
                            <c:set var="index" value="${index+1}"/>
                            <td>${index}</td>
                            <td>${exam.examname}</td>
                            <td>${exam.password}</td>
                            <td>${exam.chapter}</td>
                            <td><fmt:formatDate value="${exam.begindate}" pattern="yyyy年MM月dd日 HH时ss分"/></td>
                            <td><fmt:formatDate value="${exam.enddate}" pattern="yyyy年MM月dd日 HH时ss分"/></td>
<%--                          <jsp:setProperty name="d1" property="${exam.begindate}"/>--%>
<%--                          <jsp:setProperty name="d2" property="${exam.enddate}"/>--%>
<%--                          <td><jsp:getProperty name="d1" property=""/></td>--%>
<%--                          <jsp:setProperty name="" property=""--%>
                          <td>${exam.classId}</td>
                          <td>${exam.examtime}</td>
                          <td>${exam.examscore}</td>
                          <% request.setAttribute("currentTime", new Date()); %>
                          <c:choose>
                            <c:when test="${exam.enddate < currentTime}">
                              <td ><font color="red">已结束</font></td>
                            </c:when>
                            <c:when test="${exam.begindate >currentTime }"><td > <font color="yellow">未开始</font></td></c:when>
                            <c:otherwise>
                              <td ><font color="green">进行中</font></td>
                            </c:otherwise>
                          </c:choose>
                          <td><a href="<%=path%>/teacher/paperInfoAll?examid=${exam.id}" target="_blank">详情</a></td>
                        </tr>
                    </c:forEach>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="<%= basePath%>resources/js/jquery-3.2.1.min.js"></script>
    <script src="<%= basePath%>resources/js/popper.min.js"></script>
    <script src="<%= basePath%>resources/js/bootstrap.min.js"></script>
    <script src="<%= basePath%>resources/js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="<%= basePath%>resources/js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
    <!-- Data table plugin-->
    <script type="text/javascript" src="<%= basePath%>resources/js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<%= basePath%>resources/js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">$('#coursesTable').DataTable();</script>
  </body>
</html>