package com.mapper;

import com.bean.Student;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

import java.util.List;

public interface StuMapper {
    List<Student> selectStuList();

    void insertStu(Student student);

    Student selectStuById(@Param("id") String id);

    void updateScredit(@Param("Sno") String Sno,@Param("Cno") String Cno);

    void deleteStu(@Param("id") String id);

    void updateStu(@Param("student") Student student);

    void updateScreditBack(@Param("Sno") String id,@Param("Cno") String courseId);

    void insertPwd(@Param("student") Student student);

    void updatePwd(@Param("id") String id,@Param("newPwd") String newPwd);
}
