package com.service.impl;

import com.mapper.ClassMapper;
import com.service.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassServiceImpl implements ClassService {
    @Autowired
    ClassMapper classMapper;
    @Override
    public List<Class> getClassInfo() {
        return classMapper.selectClass();
    }

    @Override
    public void addClass(String departmentID, String classID) {
        classMapper.insertClass(departmentID, classID);
    }

    @Override
    public void removeClass(String id) {
        classMapper.deleteClass(id);
    }

    @Override
    public void updateClass(String id, String departmentId, String oldId) {
        classMapper.updateClassDno(departmentId, oldId);
        classMapper.updateClassno(id,oldId);
    }
}
