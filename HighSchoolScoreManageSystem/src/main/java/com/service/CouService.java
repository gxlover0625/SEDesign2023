package com.service;

import com.bean.Course;
import com.bean.StuCourse;
import com.bean.TeaCourse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CouService {
    List<Course> getCourseList();

    void addCourse(Course course);

    List<Course> getClassCouList();

    void addClassCourse(String classNo, String id);

    List<Course> getStuCourse(String id);

    List<Course> getCouListByClass(String className);

    void chooseCou(String id, String courseId);

    void dropCou(String id, String courseId);

    List<TeaCourse> getTeaCouList(String id);

    void removeCou(String id);

    Course getCouById(String id);

    void updateCou(Course course);

    void removeClassCou(String classNo, String id);

    List<String> selectClassidByTid(String tid);
}
