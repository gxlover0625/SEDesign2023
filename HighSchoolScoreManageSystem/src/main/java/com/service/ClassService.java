package com.service;

import java.util.List;

public interface ClassService {
    List<Class> getClassInfo();

    void addClass(String departmentID, String classID);

    void removeClass(String id);

    void updateClass(String id, String departmentId, String oldId);
}
