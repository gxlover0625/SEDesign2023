package com.controller;

import com.alibaba.fastjson.JSONArray;
import com.bean.*;
import com.mapper.CouMapper;
import com.service.*;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/student")
public class StuController {
    @Autowired
    CouService couService;

    @Autowired
    ScoreService scoreService;

    @Autowired
    LoginService loginService;

    @Autowired
    StuService stuService;

    @Autowired
    CheckService checkService;

    @Autowired
    DepService depService;

    @Autowired
    TeaService teaService;

    @Autowired
    ArticleService articleService;

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    //到学生主页
    ModelAndView getInfo(ModelAndView modelAndView, HttpSession session){
        if(session.getAttribute("stuScoreList") == null){
            Student student = (Student) session.getAttribute("student");
            List<Score> stuScoreList = scoreService.getStuScore(student.getID());
            session.setAttribute("stuScoreList", stuScoreList);
        }
        List<Score> stuScoreList = (List<Score>) session.getAttribute("stuScoreList");
        List<String> subjectList = new ArrayList<>();
        List<String> scoreList = new ArrayList<>();
        for(Score score: stuScoreList){
            subjectList.add(score.getName());
            scoreList.add(Integer.toString(score.getScore()));
        }
        JSONArray subject = (JSONArray) JSONArray.toJSON(subjectList);
        JSONArray score = (JSONArray) JSONArray.toJSON(scoreList);
        session.setAttribute("subject", subject);
        session.setAttribute("score", score);
        modelAndView.setViewName("/student/index");
        return modelAndView;
    }

    @RequestMapping(value = "/courses")
    //查看学生课程
    ModelAndView getStuCourses(ModelAndView modelAndView, HttpSession session){
        if(session.getAttribute("stuCourseList") == null){
            Student student = (Student)session.getAttribute("student");
            String ID = student.getID();
            List<Course> stuCourseList = couService.getStuCourse(ID);
            session.setAttribute("stuCourseList", stuCourseList);
        }
        modelAndView.setViewName("/student/courses");
        return modelAndView;
    }

    @RequestMapping(value = "/score", method = RequestMethod.GET)
    //查看成绩
    ModelAndView getStuScore(ModelAndView modelAndView, HttpSession session){
        if(session.getAttribute("stuScoreList") == null){
            Student student = (Student) session.getAttribute("student");
            List<Score> stuScoreList = scoreService.getStuScore(student.getID());
            session.setAttribute("stuScoreList", stuScoreList);
        }
        modelAndView.setViewName("/student/score");
        return modelAndView;
    }
    @RequestMapping(value = "/classCourse", method = RequestMethod.GET)
    //选课列表页面
    ModelAndView chooseCou(ModelAndView modelAndView, HttpSession session){
        Student student = (Student)session.getAttribute("student");
        String ID = student.getID();
        String className = student.getClassName();
        if(session.getAttribute("stuCourseList") == null){
            List<Course> stuCourseList = couService.getStuCourse(ID);
            session.setAttribute("stuCourseList", stuCourseList);
        }
        if(session.getAttribute("stuScoreList") == null){
            List<Score> stuScoreList = scoreService.getStuScore(student.getID());
            session.setAttribute("stuScoreList", stuScoreList);
        }
        List<Course> stuCourseList = (List<Course>) session.getAttribute("stuCourseList");
        List<Course> classCourseList = couService.getCouListByClass(className);
        for (Course classCourse :
                classCourseList) {
            for (Course stuCourse :
                    stuCourseList) {
                if (classCourse.getName().equals(stuCourse.getName())) {
                    classCourse.setStatus("已选");
                }
            }
        }
        //已经有成绩的课程就显示已修
        List<Score> stuScoreList = (List<Score>) session.getAttribute("stuScoreList");
        for (Course classCourse :
                classCourseList) {
            for (Score score :
                    stuScoreList) {
                if (classCourse.getName().equals(score.getName())) {
                    classCourse.setStatus("已修");
                }
            }
        }
        session.setAttribute("courseList", classCourseList);
        modelAndView.setViewName("/student/choose-courses");
        return modelAndView;
    }

    @RequestMapping(value = "/chooseCourse", method = RequestMethod.POST)
    //选择课程
    ModelAndView chooseCou(ModelAndView modelAndView, String courseId, HttpSession session, HttpServletRequest request){
        Student student = (Student)session.getAttribute("student");
        String ID = student.getID();
        String contextPath = request.getContextPath();
        couService.chooseCou(ID, courseId);
        modelAndView.setViewName("redirect:"+contextPath+"/student/classCourse");
        session.removeAttribute("stuCourseList");
        return modelAndView;
    }

    @RequestMapping(value = "/deleteCourse", method = RequestMethod.POST)
        //退选课程
    ModelAndView deleteCou(ModelAndView modelAndView, String courseId, HttpSession session, HttpServletRequest request){
        Student student = (Student)session.getAttribute("student");
        String ID = student.getID();
        String contextPath = request.getContextPath();
        couService.dropCou(ID, courseId);
        modelAndView.setViewName("redirect:"+contextPath+"/student/classCourse");
        session.removeAttribute("stuCourseList");
        return modelAndView;
    }

    @RequestMapping(value = "/updatePwd", method = RequestMethod.GET)
        //跳转到修改密码页面
    ModelAndView updatePwd_reqForward(ModelAndView modelAndView){

        modelAndView.setViewName("/student/updatePwd");
        return modelAndView;
    }

    @RequestMapping(value = "/updatePwd", method = RequestMethod.POST)
    //修改密码
    ModelAndView updatePwd(ModelAndView modelAndView, String ID, String oldPwd, String newPwd){
        Student student = loginService.checkStuPassword(ID,oldPwd);
        if(student == null){
            modelAndView.addObject("pwdMessage", "原密码错误！");
            modelAndView.setViewName("/student/updatePwd");
            return modelAndView;
        }
        stuService.updatePwd(ID, newPwd);
        modelAndView.addObject("pwdMessage", "修改成功！");
        modelAndView.setViewName("/student/updatePwd");
        return modelAndView;
    }

    @RequestMapping(value = "/check")
        //进入课程签到页面
    ModelAndView check(ModelAndView modelAndView, HttpSession session){
        Student student = (Student)session.getAttribute("student");
        String ID = student.getID();
        List<CheckRecord> recordList = checkService.getRecordListBySno(ID);
        for(CheckRecord c: recordList){
            if(c.getCheckTime() != null){
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy 年 MM 月 dd 日 HH 时 mm 分 ss 秒");
                String checkTime = sdf.format(new Date(c.getCheckTime()));
                c.setCheckDate(checkTime);
            }
            long cur = System.currentTimeMillis();
            if (c.getEndTime() < cur){
                c.setIsExpire("1");
            }else
                c.setIsExpire("0");
        }
        modelAndView.addObject("stuRecordList", recordList);

        modelAndView.setViewName("/student/check");
        return modelAndView;
    }

    @RequestMapping(value = "/updateRecord")
        //进行签到
    ModelAndView updateRecord(ModelAndView modelAndView, String Cid, String pwd, HttpSession session, HttpServletRequest request){
        Student student = (Student)session.getAttribute("student");
        String ID = student.getID();
        long cur = System.currentTimeMillis();
        //验证密码
        Check c = checkService.getCheckByCid(Cid);
        if(!c.getPwd().equals(pwd)){
            String contextPath = request.getContextPath();
            modelAndView.setViewName("redirect:"+ contextPath +"/student/check");
            return modelAndView;
        }

        checkService.updateRecord(ID, Cid, String.valueOf(cur));

        String contextPath = request.getContextPath();
        modelAndView.setViewName("redirect:"+ contextPath +"/student/check");
        return modelAndView;
    }

    @RequestMapping(value = "/article")
        //进入论坛
    ModelAndView article(ModelAndView modelAndView, HttpSession session){
        Student student = (Student)session.getAttribute("student");
        String ID = student.getID();
        if(session.getAttribute("departmentList") == null){
            List<Department> departmentList = depService.getDepInfo();
            //放到session作用域中，方便后面重复使用
            session.setAttribute("departmentList", departmentList);
        }
        if(session.getAttribute("teacherList") == null){
            List<Teacher> teacherList = teaService.getTeaList();
            session.setAttribute("teacherList", teacherList);
        }
        if(session.getAttribute("courseList") == null){
            List<Course> courseList = couService.getCourseList();
            session.setAttribute("courseList", courseList);
        }
        List<Article> articleList = articleService.getArticleList();

        for(Article a: articleList){
            if(a.getPubDate() != null){
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy 年 MM 月 dd 日 HH 时 mm 分 ss 秒");
                String pubTime = sdf.format(new Date(a.getPubDate()));
                a.setPubTime(pubTime);
            }
        }
            //放到session作用域中，方便后面重复使用
        session.setAttribute("articleList", articleList);

        modelAndView.setViewName("/student/article");
        return modelAndView;
    }
    @RequestMapping(value = "/addArticle", method = RequestMethod.GET)
        //发布文章页面
    ModelAndView addArticle(ModelAndView modelAndView, HttpSession session){
        if(session.getAttribute("departmentList") == null){
            List<Department> departmentList = depService.getDepInfo();
            //放到session作用域中，方便后面重复使用
            session.setAttribute("departmentList", departmentList);
        }
        if(session.getAttribute("teacherList") == null){
            List<Teacher> teacherList = teaService.getTeaList();
            session.setAttribute("teacherList", teacherList);
        }
        if(session.getAttribute("courseList") == null){
            List<Course> courseList = couService.getCourseList();
            session.setAttribute("courseList", courseList);
        }
        modelAndView.setViewName("/student/addArticle");
        return modelAndView;
    }

    @RequestMapping(value = "/insertArticle")
        //添加文章
    ModelAndView insertArticle(ModelAndView modelAndView, String title, String content, String departmentID, String courseID, String teacherID, HttpServletRequest request, HttpSession session){
        Student student = (Student)session.getAttribute("student");
        String ID = student.getID();
        long pubDate = System.currentTimeMillis();
        articleService.insertArticle(ID, title, content, departmentID,  courseID, teacherID, String.valueOf(pubDate));
        String contextPath = request.getContextPath();
        modelAndView.setViewName("redirect:"+ contextPath +"/student/article");
        return modelAndView;
    }

    @RequestMapping(value = "/myArticle", method = RequestMethod.GET)
        //我的发言页面
    ModelAndView myArticle(ModelAndView modelAndView, HttpSession session){
        Student student = (Student)session.getAttribute("student");
        String ID = student.getID();

        List<Article> myArticleList = articleService.getArticleListBySno(ID);
        modelAndView.addObject("myArticleList", myArticleList);
        for(Article a: myArticleList){
            if(a.getPubDate() != null){
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy 年 MM 月 dd 日 HH 时 mm 分 ss 秒");
                String pubTime = sdf.format(new Date(a.getPubDate()));
                a.setPubTime(pubTime);
            }
        }
        modelAndView.setViewName("/student/myArticle");
        return modelAndView;
    }

    @RequestMapping(value = "/deleteArticle")
        //删除我的发言
    ModelAndView deleteArticle(ModelAndView modelAndView, String ID, HttpServletRequest request, HttpSession session){

        articleService.deleteArticleListByAid(ID);

        String contextPath = request.getContextPath();
        modelAndView.setViewName("redirect:"+ contextPath +"/student/myArticle");
        return modelAndView;
    }

    @RequestMapping(value = "/updateArticle", method = RequestMethod.GET)
        //修改我的发言页面
    ModelAndView updateArticle(ModelAndView modelAndView, String ID, HttpSession session){
        if(session.getAttribute("departmentList") == null){
            List<Department> departmentList = depService.getDepInfo();
            //放到session作用域中，方便后面重复使用
            session.setAttribute("departmentList", departmentList);
        }
        if(session.getAttribute("teacherList") == null){
            List<Teacher> teacherList = teaService.getTeaList();
            session.setAttribute("teacherList", teacherList);
        }
        if(session.getAttribute("courseList") == null){
            List<Course> courseList = couService.getCourseList();
            session.setAttribute("courseList", courseList);
        }
        Article article = articleService.getArticleByID(ID);
        modelAndView.addObject("article", article);

        modelAndView.setViewName("/student/updateArticle");
        return modelAndView;
    }

    @RequestMapping(value = "/updateArticle", method = RequestMethod.POST)
        //修改我的发言页面
    ModelAndView updateArticle(ModelAndView modelAndView, Article article, HttpServletRequest request, HttpSession session){
        articleService.updateArticleById(article);
        String contextPath = request.getContextPath();
        modelAndView.setViewName("redirect:"+ contextPath +"/student/myArticle");
        return modelAndView;
    }

    @RequestMapping(value = "/findArticle")
        //文章检索
    ModelAndView findArticle(ModelAndView modelAndView, String keywords, String departmentID, String courseID, String teacherID, HttpSession session){
        if(session.getAttribute("departmentList") == null){
            List<Department> departmentList = depService.getDepInfo();
            //放到session作用域中，方便后面重复使用
            session.setAttribute("departmentList", departmentList);
        }
        if(session.getAttribute("teacherList") == null){
            List<Teacher> teacherList = teaService.getTeaList();
            session.setAttribute("teacherList", teacherList);
        }
        if(session.getAttribute("courseList") == null){
            List<Course> courseList = couService.getCourseList();
            session.setAttribute("courseList", courseList);
        }
        List<Article> articleList = articleService.getArticleListByKeywords(keywords, departmentID, courseID, teacherID);
        //放到session作用域中，方便后面重复使用
        session.setAttribute("articleList", articleList);

        modelAndView.setViewName("/student/article");
        return modelAndView;
    }
}
