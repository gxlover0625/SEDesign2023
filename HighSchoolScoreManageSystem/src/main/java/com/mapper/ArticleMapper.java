package com.mapper;

import com.bean.Article;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ArticleMapper {
    void insertArticle(@Param("id") String id,@Param("title") String title,
                       @Param("content") String content,@Param("departmentID") String departmentID,
                       @Param("courseID") String courseID,@Param("teacherID") String teacherID,
                       @Param("pubDate") String pubdate);

    List<Article> selectArticleList();

    List<Article> selectArticleListBySno(@Param("id") String id);

    void deleteArticleById(@Param("id") String id);

    Article selectArticleById(@Param("id") String id);

    void updateArticle(@Param("article") Article article);

    List<Article> selectArticleListByKeywords(@Param("keywords") String keywords,
                                              @Param("Dno") String departmenID,
                                              @Param("Cno") String courseID,
                                              @Param("Tno") String teacherID);
}
