package com.service.impl;

import com.bean.Judge;
import com.mapper.JudgeMapper;
import com.service.JudgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class JudgeServiceImpl implements JudgeService {
    @Autowired
    JudgeMapper judgeMapper;

    @Override
    public List<Judge> queryAllJudge() {
        return judgeMapper.queryAllJudge();
    }

    @Override
    public List<Judge> queryAllJudgeByTid(String tid) {
        return judgeMapper.queryAllJudgeByTid(tid);
    }

    @Override
    public List<Judge> queryJudgeByCid(String cid) {
        return judgeMapper.queryJudgeByCid(cid);
    }

    @Override
    public int insertJudge(Map<String, String> map) {
        return judgeMapper.insertJudge(map);
    }

    @Override
    public int deleteJudgeById(String jid) {
        return judgeMapper.deleteJudgeById(jid);
    }

    @Override
    public Judge queryJudgeById(String jid,String tid) {
        return judgeMapper.queryJudgeById(jid,tid);
    }

    @Override
    public List<Judge> queryJudgeByExamId(String exmaid) {
        return judgeMapper.queryJudgeByExamId(exmaid);
    }
}
