package com.service;

import com.bean.Student;

import java.util.List;

public interface StuService {
    List<Student> getStuInfo();

    void addStu(Student student);

    Student getStuById(String id);

    void removeStu(String id);

    void updateStu(Student student);

    void updatePwd(String id, String newPwd);
}
