package com.service;

import com.bean.MyStudent;
import com.bean.Score;
import com.bean.Student;
import com.bean.Teacher;

import java.util.List;

public interface TeaService {
    List<Teacher> getTeaList();

    void addTea(Teacher teacher);

    Teacher getTeaById(String id);

    List<MyStudent> getMyStuList(String id);

    void insertScore(String tno, String[] id, String[] term, String[] courseId, String[] score);

    void removeTea(String id);

    void updateTea(Teacher teacher);

    void updatePwd(String id, String newPwd);

    void insertCheck(String tno, String Cno, String ClassNo, long beginTime, long endTime, String pwd);
}
