package com.controller;

import com.alibaba.fastjson.JSONArray;
import com.bean.*;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


@Controller
@RequestMapping("/teacher")
public class TeaController {
    @Autowired
    TeaService teaService;

    @Autowired
    CouService couService;

    @Autowired
    ScoreService scoreService;

    @Autowired
    StuService stuService;

    @Autowired
    LoginService loginService;

    @Autowired
    CheckService checkService;

    @Autowired
    PointService pointService;

    @Autowired
    JudgeService judgeService;

    @Autowired
    ChoiceService choiceService;

    @Autowired
    ExamService examService;

    @Autowired
    ExamPaperService paperService;


    @RequestMapping(value = "/index", method = RequestMethod.GET)
    //跳转教师信息页面
    ModelAndView getTeaInfo(ModelAndView modelAndView, HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        String ID = teacher.getID();
        if(session.getAttribute("teaScoreList") == null){
            List<Score> teaScoreList = scoreService.getMyStuScore(ID);
            session.setAttribute("teaScoreList", teaScoreList);
        }
        List<Score> teaScoreList = (List<Score>) session.getAttribute("teaScoreList");
        List<String> subjectList = new ArrayList<>();
        List<String> scoreList = new ArrayList<>();
        Map<String, Integer> mp = new HashMap<>();
        Map<String, Integer> cnt = new HashMap<>();
        for(Score sc: teaScoreList){
            if(!subjectList.contains(sc.getName()))
                subjectList.add(sc.getName());
            mp.put(sc.getName(), mp.get(sc.getName()) == null? sc.getScore():mp.get(sc.getName()) + sc.getScore());
            cnt.put(sc.getName(), cnt.get(sc.getName()) == null? 1:cnt.get(sc.getName()) + 1);
        }
        for(String subject: subjectList){
            scoreList.add(Integer.toString(mp.get(subject) / cnt.get(subject)));
        }
        JSONArray subject = (JSONArray) JSONArray.toJSON(subjectList);
        JSONArray score = (JSONArray) JSONArray.toJSON(scoreList);
        session.setAttribute("subject", subject);
        session.setAttribute("score", scoreList);
        modelAndView.setViewName("/teacher/index");
        return modelAndView;
    }

    @RequestMapping(value = "/courses", method = RequestMethod.GET)
    //教师任课信息
    ModelAndView getTeaCou(ModelAndView modelAndView, HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        String ID = teacher.getID();

        List<TeaCourse> teaCourseList = couService.getTeaCouList(ID);
        session.setAttribute("teaCourseList", teaCourseList);

        modelAndView.setViewName("/teacher/courses");

        return modelAndView;
    }

    @RequestMapping(value = "/myStudent", method = RequestMethod.GET)
    //获取教师的学生
    ModelAndView getTeaStuList(ModelAndView modelAndView, HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        String ID = teacher.getID();
        if(session.getAttribute("myStudentList") == null){
            List<MyStudent> myStudentList = teaService.getMyStuList(ID);
            session.setAttribute("myStudentList", myStudentList);
        }
        modelAndView.setViewName("/teacher/myStudent");
        return modelAndView;
    }

    @RequestMapping(value = "score", method = RequestMethod.GET)
    //获取自己学生的成绩
    ModelAndView getMyStuScore(ModelAndView modelAndView, HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        String ID = teacher.getID();
        if(session.getAttribute("teaScoreList") == null){
            List<Score> teaScoreList = scoreService.getMyStuScore(ID);
            session.setAttribute("teaScoreList", teaScoreList);
        }
        List<Score> teaScoreList = (List<Score>) session.getAttribute("teaScoreList");
        int avgScore = 0;
        for(Score sc: teaScoreList){
            avgScore += sc.getScore();
        }
        if(teaScoreList.size() != 0) session.setAttribute("avgScore", avgScore / teaScoreList.size());
        else session.setAttribute("avgScore", 0);
        modelAndView.setViewName("/teacher/score");
        return modelAndView;
    }

    @RequestMapping(value = "scoreInsert", method = RequestMethod.GET)
    //获取该教师待录入成绩列表
    ModelAndView scoreInsert(ModelAndView modelAndView, HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        String ID = teacher.getID();
        if(session.getAttribute("myStudentList") == null){
            List<MyStudent> myStudentList = teaService.getMyStuList(ID);
            session.setAttribute("myStudentList", myStudentList);
        }
        if(session.getAttribute("teaScoreList") == null){
            List<Score> teaScoreList = scoreService.getMyStuScore(ID);
            session.setAttribute("teaScoreList", teaScoreList);
        }
        if(session.getAttribute("myStuToInsertScoreList") == null){
            List<MyStudent> myStudentList = (List<MyStudent>) session.getAttribute("myStudentList");
            List<Score> teaScoreList = (List<Score>) session.getAttribute("teaScoreList");
            Iterator<MyStudent> iterator = myStudentList.iterator();
            while (iterator.hasNext()) {
                MyStudent myStudent = iterator.next();
                for (Score score:
                        teaScoreList) {
                    if (myStudent.getID().equals(score.getID()) && myStudent.getCourseName().equals(score.getName()) && myStudent.getTerm().equals(score.getTerm())) {
                        iterator.remove();
                    }
                }
            }
            session.setAttribute("myStuToInsertScoreList", myStudentList);
        }
        modelAndView.setViewName("/teacher/scoreInsert");
        return modelAndView;
    }

    @RequestMapping(value = "/insertScore", method = RequestMethod.POST)
    //录入成绩
    ModelAndView insertSc(ModelAndView modelAndView, String[] ID, String[] term, String[] courseId, String[] score, HttpServletRequest request, HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        String Tno = teacher.getID();
        teaService.insertScore(Tno, ID, term, courseId, score);
        session.removeAttribute("myStuToInsertScoreList");
        session.removeAttribute("teaScoreList");
        String contextPath = request.getContextPath();
        modelAndView.setViewName("redirect:"+ contextPath +"/teacher/score");
        return modelAndView;
    }

    @RequestMapping(value = "/deleteStuScore")
    //退回学生成绩
    ModelAndView deleteStuScore(ModelAndView modelAndView, String ID, String courseId, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        String contextPath = request.getContextPath();
        scoreService.removeStuScore(ID, courseId);
        session.removeAttribute("myStudentList");
        session.removeAttribute("myStuToInsertScoreList");
        session.removeAttribute("teaScoreList");
        modelAndView.setViewName("redirect:" + contextPath + "/teacher/scoreInsert");
        return modelAndView;
    }

    @RequestMapping(value = "/updatePwd", method = RequestMethod.GET)
        //跳转到修改密码页面
    ModelAndView updatePwd_reqForward(ModelAndView modelAndView){

        modelAndView.setViewName("/teacher/updatePwd");
        return modelAndView;
    }

    @RequestMapping(value = "/updatePwd", method = RequestMethod.POST)
        //修改密码
    ModelAndView updatePwd(ModelAndView modelAndView, String ID, String oldPwd, String newPwd){
        Teacher teacher = loginService.checkTeaPassword(ID,oldPwd);
        if(teacher == null){
            modelAndView.addObject("pwdMessage", "原密码错误！");
            modelAndView.setViewName("/teacher/updatePwd");
            return modelAndView;
        }
        teaService.updatePwd(ID, newPwd);
        modelAndView.addObject("pwdMessage", "修改成功！");
        modelAndView.setViewName("/teacher/updatePwd");
        return modelAndView;
    }

    @RequestMapping(value = "/check", method = RequestMethod.GET)
        //进入发起签到页面
    ModelAndView check(ModelAndView modelAndView, HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        String ID = teacher.getID();

        List<TeaCourse> teaCourseList = couService.getTeaCouList(ID);
        session.setAttribute("teaCourseList", teaCourseList);

        for(TeaCourse c: teaCourseList){
            c.setName(c.getName()+"("+c.getClassName()+")");
            c.setID(c.getID()+"("+c.getClassName()+")");
        }
        modelAndView.addObject("teacherCourseList", teaCourseList);

        List<Check> checkList = checkService.getCheckList(ID);
        for(Check c: checkList){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy 年 MM 月 dd 日 HH 时 mm 分 ss 秒");
            String begin = sdf.format(new Date(c.getBeginTime()));
            c.setBeginDate(begin);
            String end = sdf.format(new Date(c.getEndTime()));
            c.setEndDate(end);
        }
        modelAndView.addObject("checkList", checkList);

        modelAndView.setViewName("/teacher/check");
        return modelAndView;
    }

    @RequestMapping(value = "/insertCheck", method = RequestMethod.POST)
        //发起签到
    ModelAndView insertCheck(ModelAndView modelAndView, String Cno, String pwd, String time, HttpSession session, HttpServletRequest request){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        String Tno = teacher.getID();
        long beginTime = System.currentTimeMillis();
        long endTime = beginTime + Integer.parseInt(time) * 60000L;
        int Lindex = Cno.indexOf('(');
        int Rindex = Cno.indexOf(')');
        String ClassNo = Cno.substring(Lindex + 1, Rindex);
        Cno = Cno.substring(0, Lindex);
        teaService.insertCheck(Tno, Cno, ClassNo, beginTime, endTime, pwd);

        if(session.getAttribute("myStudentList") == null){
            List<MyStudent> myStudentList = teaService.getMyStuList(Tno);
            session.setAttribute("myStudentList", myStudentList);
        }

        String Cid = checkService.getCidbyBeginTime(beginTime);
        List<MyStudent> myStudentList = (List<MyStudent>) session.getAttribute("myStudentList");
        for(MyStudent student: myStudentList){
            if(student.getCourseId().equals(Cno))
                checkService.insertRecord(Cid, student.getID());
        }

        String contextPath = request.getContextPath();
        modelAndView.setViewName("redirect:"+ contextPath +"/teacher/check");
        return modelAndView;
    }
    @RequestMapping(value = "/selectCheck", method = RequestMethod.GET)
        //进入查看签到详情页面
    ModelAndView selectCheck(ModelAndView modelAndView, String Cid, HttpSession session){
            List<CheckRecord> recordList = checkService.getRecordList(Cid);
        for(CheckRecord c: recordList){
            long cur = System.currentTimeMillis();
            if (c.getEndTime() < cur){
                c.setIsExpire("1");
            }
            else
                c.setIsExpire("0");
            if(c.getCheckTime() != null){
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy 年 MM 月 dd 日 HH 时 mm 分 ss 秒");
                String checkTime = sdf.format(new Date(c.getCheckTime()));
                c.setCheckDate(checkTime);
            }

        }
        modelAndView.addObject("recordList", recordList);

        modelAndView.setViewName("/teacher/record");
        return modelAndView;
    }

    @RequestMapping(value = "/handCheck")
        //手动签到
    ModelAndView handCheck(ModelAndView modelAndView, String Cid, String Sno, HttpSession session, HttpServletRequest request){
        long cur = System.currentTimeMillis();

        checkService.updateRecord(Sno, Cid, String.valueOf(cur));

        String contextPath = request.getContextPath();
        modelAndView.setViewName("redirect:"+ contextPath +"/teacher/selectCheck?Cid=" + Cid);
        return modelAndView;
    }

    /**
     * 查询该老师拥有的所有知识点
     * @param modelAndView
     * @param session
     * @param request
     * @return
     */
    @RequestMapping(value = "/kpoints")
    ModelAndView selectKpoints(ModelAndView modelAndView, HttpSession session, HttpServletRequest request){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        List<Point1> allPoint1 = pointService.findAllPoint1ByTeaId(teacher.getID());
        modelAndView.addObject("point1",allPoint1);
        modelAndView.setViewName("/teacher/kpoints");
        return modelAndView;
    }

    @RequestMapping(value = "/allpoint1")
    @ResponseBody
    List<Point1> queryAllPoint1(Model model,HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        List<Point1> points = pointService.findAllPoint1ByTeaId(teacher.getID());
        model.addAttribute("point",points);
        return points;
    }

    @RequestMapping(value = "/addpoint1")
    ModelAndView addKpoint(ModelAndView modelAndView,HttpSession session,HttpServletRequest request,String pointname){
//        System.out.println(pointname);
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        String contextPath=request.getContextPath();
        pointService.insertPoint1(pointname);
        String cid = String.valueOf(pointService.getLastId());
        pointService.insertPointOwner(cid,teacher.getID());
        modelAndView.setViewName("redirect:"+ contextPath +"/teacher/kpoints");
        return modelAndView;
    }

    @RequestMapping(value = "/addpoint2")
    ModelAndView addKpoint2(ModelAndView modelAndView,HttpSession session,HttpServletRequest request,String aid,String pname){
        String contextPath=request.getContextPath();
        pointService.insertPoint2(aid,pname);
        modelAndView.setViewName("redirect:"+ contextPath +"/teacher/kpoints");
        return modelAndView;
    }

    @RequestMapping(value = "/delepoint1")
    ModelAndView deletePoint1(ModelAndView modelAndView,HttpSession session,HttpServletRequest request,String id){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        pointService.deletePoint1(id,teacher.getID());
        String contextPath=request.getContextPath();
        modelAndView.setViewName("redirect:"+ contextPath +"/teacher/kpoints");
        return modelAndView;
    }

    @RequestMapping(value = "/delepoint2")
    ModelAndView deletePoint2(ModelAndView modelAndView,HttpSession session,HttpServletRequest request,String chapter,String chaptertwo){
        pointService.deletePoint2(chaptertwo);
        String contextPath=request.getContextPath();
        modelAndView.setViewName("redirect:"+ contextPath +"/teacher/kpoints");
        return modelAndView;
    }

    @RequestMapping(value = "/tk")
    ModelAndView queryTKPage(ModelAndView modelAndView, HttpSession session, HttpServletRequest request, @RequestParam(defaultValue = "1")int pn){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        PageHelper.startPage(pn,4);
        System.out.println(pn);

        List<Judge> judges = judgeService.queryAllJudgeByTid(teacher.getID());
        PageInfo pageInfo=new PageInfo(judges,5);
        session.setAttribute("pageInfo",pageInfo);
        session.setAttribute("judges",judges);
        modelAndView.setViewName("/teacher/tk");
        return modelAndView;
    }

    @RequestMapping(value = "/createjudge")
    ModelAndView addJudge(ModelAndView modelAndView,HttpSession session,HttpServletRequest request,String content,
                          String answer,String analysis,String chapter,String chaptertwo,String difficulty){
        Map<String,String> map=new HashMap<>();
        map.put("cnt",content);
        map.put("ans",answer);
        map.put("als",analysis);
        map.put("bid",chaptertwo);
        map.put("dif",difficulty);
        judgeService.insertJudge(map);
        String contextPath=request.getContextPath();
        modelAndView.setViewName("redirect:"+ contextPath +"/teacher/tk");
        return modelAndView;
    }

    @RequestMapping(value = "/deletejudge")
    ModelAndView deleteJudgeById(ModelAndView modelAndView,HttpSession session,HttpServletRequest request,String jid){
        judgeService.deleteJudgeById(jid);
        System.out.println(jid);
        String contextPath=request.getContextPath();
        modelAndView.setViewName("redirect:"+ contextPath +"/teacher/tk");
        return modelAndView;
    }

    @ResponseBody
    @RequestMapping(value = "/judgeId")
    Judge queryJudgeById(HttpSession session,@RequestBody Judge qjudge){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        System.out.println(qjudge);
        Judge judge = judgeService.queryJudgeById(String.valueOf(qjudge.getId()),teacher.getID());
        System.out.println("----------------");

        System.out.println("id="+qjudge.getId());
        System.out.println(judge);
        System.out.println("----------------");
        return judge;
    }

    //测试数据库用
    @ResponseBody
    @RequestMapping(value = "/allchoice")
    List<Choice> queryAllChoice(HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        return choiceService.queryAllChoiceByTid(teacher.getID());
    }

    @RequestMapping("/tkchoice")
    ModelAndView tkChoiceIndex(ModelAndView modelAndView,HttpSession session,HttpServletRequest request){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        List<Choice> choiceList = choiceService.queryAllChoiceByTid(teacher.getID());
        session.setAttribute("choices",choiceList);

        List<Point1> point1s = pointService.findAllPoint1ByTeaId(teacher.getID());
        session.setAttribute("point1list",point1s);
        session.setAttribute("point2list",point1s.get(0).getPoint2());
        modelAndView.setViewName("/teacher/tkchoice");
        return modelAndView;
    }

    @RequestMapping("/querypoint2")
    @ResponseBody
    List<Point2> queryPoint2ByPoint1Id(HttpSession session,String id){
        List<Point2> point2s = pointService.queryPoint2ByPoint1Id(id);
        session.setAttribute("point2list",point2s);
        return point2s;
    }

    @RequestMapping("/addchoice")
    ModelAndView addChoice(ModelAndView modelAndView,HttpSession session,HttpServletRequest request,Choice choice){
        String contextPath=request.getContextPath();
        choiceService.insertChoice(choice);
        modelAndView.setViewName("redirect:"+ contextPath +"/teacher/tkchoice");
        return modelAndView;
    }

    @RequestMapping("/deleteChoice")
    ModelAndView deleteChoice(ModelAndView modelAndView,HttpSession session,HttpServletRequest request,String chid){
        String contextPath=request.getContextPath();
        choiceService.deleteChoice(chid);
        modelAndView.setViewName("redirect:"+ contextPath +"/teacher/tkchoice");
        return modelAndView;
    }

    @RequestMapping("/examinfo")
    ModelAndView examInfoIndex(ModelAndView modelAndView,HttpSession session,HttpServletRequest request){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        List<ExamInfomation> exams = examService.queryExamByTid(teacher.getID());
        List<TeaCourse> teaCourseList = couService.getTeaCouList(teacher.getID());

        List<String> ClassidList=new ArrayList<>();
        for (int i = 0; i < teaCourseList.size(); i++) {
            ClassidList.add(teaCourseList.get(i).getClassName());
        }
        LinkedHashSet<String> hashSet=new LinkedHashSet<>(ClassidList);
        List<String> OutputClassidList=new ArrayList<>(hashSet);
        session.setAttribute("ClassidList", OutputClassidList);

        for (int i = 0; i < exams.size(); i++) {
            int examscore=exams.get(i).getChoicenum()*exams.get(i).getChoicescore()+exams.get(i).getJudgenum()*exams.get(i).getJudgescore();
            exams.get(i).setExamscore(examscore);
            long d1=exams.get(i).getBegindate().getTime();
            long d2=exams.get(i).getEnddate().getTime();
            exams.get(i).setExamtime((int)((d2-d1)/1000/60));
        }

        List<Point1> points = pointService.findAllPoint1ByTeaId(teacher.getID());
        session.setAttribute("pointselects",points);

        session.setAttribute("exmalist",exams);
        modelAndView.setViewName("/teacher/examinfo");
        return modelAndView;
    }

    @RequestMapping("/examtest")
    @ResponseBody
    List<ExamInfomation> queryExamInfo(HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        List<ExamInfomation> exams=examService.queryExamByTid(teacher.getID());
        for (int i = 0; i < exams.size(); i++) {
            long d1=exams.get(i).getBegindate().getTime();
            long d2=exams.get(i).getEnddate().getTime();
            exams.get(i).setExamtime((int)((d2-d1)/1000/60));
        }
        return exams;
    }

    @RequestMapping("/addExam")
    ModelAndView addExam(ModelAndView modelAndView,HttpSession session,HttpServletRequest request,
                         String examname,String password,String classid,String aid,String begindate,
                         String enddate,String choicenum,String choicescore,String judgenum,String judgescore
                         ) throws ParseException {
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        if(begindate.indexOf('T')!=-1){
            begindate+=":00";
            begindate=begindate.replace('T',' ');
        }
        System.out.println("begindate = " + begindate);

        if(enddate.indexOf('T')!=-1){
            enddate+=":00";
            enddate=enddate.replace('T',' ');
        }
        System.out.println("enddate = " + enddate);


        ExamInfomation exam=new ExamInfomation();
        exam.setExamname(examname);
        exam.setPassword(password);
        exam.setChapterId(aid);
        exam.setTeacherId(teacher.getID());
        exam.setClassId(classid);
        Date bdate=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(begindate);
        Date edate=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(enddate);
        exam.setBegindate(bdate);
        exam.setEnddate(edate);
        int examtime=(int)((edate.getTime()-bdate.getTime())/1000/60);
        exam.setExamtime(examtime);
        exam.setChoicenum(Integer.valueOf(choicenum));
        exam.setChoicescore(Integer.valueOf(choicescore));
        exam.setJudgenum(Integer.valueOf(judgenum));
        exam.setJudgescore(Integer.valueOf(judgescore));

        examService.addExam(exam);
        exam.setId(String.valueOf(examService.queryExamId()));
        genExamPaper(exam);
        String contextPath=request.getContextPath();
        modelAndView.setViewName("redirect:"+ contextPath +"/teacher/examinfo");
        return modelAndView;
    }

    /*@RequestMapping("/genjudge")
    @ResponseBody
    List<Judge> queryJudge(String examid){
        return paperService.queryJudgeListByExamId(examid);
    }

    @RequestMapping("/genchoice")
    @ResponseBody
    List<Choice> queryChoice(String examid){
        return paperService.queryChoiceListByExamId(examid);
    }*/

    //假设题目足够的情况下,进行随机选题
    void genExamPaper(ExamInfomation examInfomation){
        int need_choicenum=examInfomation.getChoicenum();//选择题个数
        int need_judgenum=examInfomation.getJudgenum();//判断题个数
        String id = examInfomation.getId();//考试的id
        String chapterId = examInfomation.getChapterId();//章节名

        //根据章节名chapterId找到所有的选择题和判断题
        List<Choice> paper_choice_list=choiceService.queryChoiceByCid(chapterId);//所有可用的选择题
        List<Judge> paper_judge_list=judgeService.queryJudgeByCid(chapterId);//所有可用的判断题


        int val_choicenum=paper_choice_list.size();//可用的选择题个数
        int val_judgenum=paper_judge_list.size();//可用的判断题个数

        int[] choose_index_choice=randomCommon(0,val_choicenum-1,need_choicenum);
        int[] choose_index_judge=randomCommon(0,val_judgenum-1,need_judgenum);

        List<Choice> paper_final_choices=new ArrayList<>();
        List<Judge>  paper_final_judges=new ArrayList<>();
        for (int i = 0; i < choose_index_choice.length; i++) {
            paper_final_choices.add(paper_choice_list.get(choose_index_choice[i]));
            paperService.addExamPaperChoice(id,String.valueOf(paper_choice_list.get(choose_index_choice[i]).getId()));
        }
        for (int i = 0; i < choose_index_judge.length; i++) {
            paper_final_judges.add(paper_judge_list.get(choose_index_judge[i]));
            paperService.addExamPaperJudge(id,String.valueOf(paper_judge_list.get(choose_index_judge[i]).getId()));
        }

    }

    public int[] randomCommon(int min, int max, int n){
        HashSet<Integer> set=new HashSet<>();
        if (n > (max - min + 1) || max < min) {
            return null;
        }
        int[] result = new int[n];
        int count = 0;
        while(count < n) {
            Random r=new Random();
            int num=r.nextInt(max-min)+min;
            set.add(num);
            int size=set.size();
            if(size>count){
                result[count++]=num;
            }
        }
        return result;
    }

    @RequestMapping("/paperInfoAll")
    ModelAndView queryPaperInfoAllByExamId(ModelAndView modelAndView,HttpSession session,HttpServletRequest request,String examid){
        List<Choice> paperchoices = choiceService.queryChoiceByExamId(examid);
        List<Judge> paperjudges = judgeService.queryJudgeByExamId(examid);
        ExamInfomation exam = examService.queryExamInfoByExamId(examid);
        session.setAttribute("examinfo",exam);
        session.setAttribute("judgelist",paperjudges);
        session.setAttribute("choicelist",paperchoices);
        modelAndView.setViewName("/teacher/exam_detail");
        return modelAndView;
    }

}
