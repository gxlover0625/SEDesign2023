package com.service;

import com.bean.ExamInfomation;
import com.bean.Judge;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface JudgeService {
    List<Judge> queryAllJudge();

    List<Judge> queryAllJudgeByTid(String tid);

    List<Judge> queryJudgeByCid(String cid);

    int insertJudge(Map<String,String> map);

    int deleteJudgeById(String jid);

    Judge queryJudgeById(String jid,String tid);

    List<Judge> queryJudgeByExamId(String exmaid);

}
