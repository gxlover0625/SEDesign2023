package com.service.impl;

import com.bean.Department;
import com.mapper.DepMapper;
import com.service.DepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepServiceImpl implements DepService {
    @Autowired
    DepMapper depMapper;
    @Override
    public List<Department> getDepInfo() {
        return depMapper.selectDepAll();
    }

    @Override
    public void addDep(String id, String name) {
        depMapper.insertDep(id, name);
    }

    @Override
    public void removeDep(String id) {
        depMapper.deleteDep(id);
    }

    @Override
    public void updateDep(String id, String name) {
        depMapper.updateDep(id, name);
    }
}
