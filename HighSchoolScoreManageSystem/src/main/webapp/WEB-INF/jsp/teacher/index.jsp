<%@ page language="java" contentType="text/html;charset=utf-8" isELIgnored="false" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>高校成绩管理系统</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="<%= basePath%>resources/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="<%= basePath%>resources/js/echarts.js"></script>
    <script type="text/javascript" src="<%= basePath%>resources/js/jquery-3.2.1.min.js"></script>
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="<%=path%>/teacher/index">教务管理系统</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="<%=basePath%>logout"><i class="fa fa-sign-out fa-lg"></i> 退出</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" style="width: 80px; height: 80px;" src="<%=basePath%>resources/img/user.png" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">${sessionScope.teacher.name}</p>
          <p class="app-sidebar__user-designation">教师</p>
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item active" href="<%=path%>/teacher/index"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">个人信息</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/courses"><i class="app-menu__icon fa fa fa-book"></i><span class="app-menu__label">我的任课</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/myStudent"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">我的学生</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/score"><i class="app-menu__icon fa fa-align-left"></i><span class="app-menu__label">学生成绩</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/scoreInsert"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">成绩录入</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/check"><i class="app-menu__icon fa fa-check"></i><span class="app-menu__label">发起签到</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/kpoints"><i class="app-menu__icon fa fa-bookmark-o"></i><span class="app-menu__label">知识点</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/tk"><i class="app-menu__icon fa fa-list-alt"></i><span class="app-menu__label">题库-判断题</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/tkchoice"><i class="app-menu__icon fa fa-list-alt"></i><span class="app-menu__label">题库-选择题</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/examinfo"><i class="app-menu__icon fa fa-history"></i><span class="app-menu__label">考试历史</span></a></li>
        <li><a class="app-menu__item" href="<%=path%>/teacher/updatePwd"><i class="app-menu__icon fa fa-certificate"></i><span class="app-menu__label">修改密码</span></a></li>

      </ul>
    </aside>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-user"></i> 个人信息</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="<%=path%>/teacher/index">个人信息</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-6 col-lg-3">
          <div class="widget-small primary coloured-icon"><i class="icon fa fa-id-card fa-3x"></i>
            <div class="info">
              <h4>教师号</h4>
              <p><b>${teacher.ID}</b></p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-3">
          <div class="widget-small danger coloured-icon"><i class="icon fa fa-mars fa-3x"></i>
            <div class="info">
              <h4>性别</h4>
              <p><b>${teacher.sex}</b></p>
            </div>
          </div>
        </div>
          <div class="col-md-6 col-lg-3">
              <div class="widget-small info coloured-icon"><i class="icon fa fa-clock-o fa-3x"></i>
                  <div class="info">
                      <h4>年龄</h4>
                      <p><b>${teacher.age}</b></p>
                  </div>
              </div>
          </div>
        <div class="col-md-6 col-lg-3">
          <div class="widget-small warning coloured-icon"><i class="icon fa fa-clock-o fa-3x"></i>
            <div class="info">
              <h4>职称</h4>
              <p><b>${teacher.title}</b></p>
            </div>
          </div>
        </div>
          <div class="col-md-6 col-lg-3">
              <div class="widget-small info coloured-icon"><i class="icon fa fa-phone fa-3x"></i>
                  <div class="info">
                      <h4>联系电话</h4>
                      <p><b>${teacher.phone}</b></p>
                  </div>
              </div>
          </div>
      </div>
      <div id="echart" style="width: 600px;height:400px;"></div>
      <script>
        var myChart = echarts.init(document.getElementById('echart'))
        option = {
          //标题
          title: {
            show: true, //显示策略，默认值true,可选为：true（显示） | false（隐藏）
            text: '教授的各科目平均分', //主标题文本，'\n'指定换行
            x: 'center', //水平安放位置，默认为'left'，可选为：'center' | 'left' | 'right' | {number}（x坐标，单位px）
            y: 'top', //垂直安放位置，默认为top，可选为：'top' | 'bottom' | 'center' | {number}（y坐标，单位px）
            // textAlign: null,//水平对齐方式，默认根据x设置自动调整，可选为： left' | 'right' | 'center
            // backgroundColor: 'rgba(0,0,0,0)', //标题背景颜色，默认'rgba(0,0,0,0)'透明
            // borderColor: '#ccc', //标题边框颜色,默认'#ccc'
            // borderWidth: 0, //标题边框线宽，单位px，默认为0（无边框）
            // padding: 5, //标题内边距，单位px，默认各方向内边距为5，接受数组分别设定上右下左边距
            // itemGap: 10, //主副标题纵向间隔，单位px，默认为10
            // textStyle: { //主标题文本样式{"fontSize": 18,"fontWeight": "bolder","color": "#333"}
            //   fontFamily: 'Arial, Verdana, sans...',
            //   fontSize: 16,
            //   fontStyle: 'normal',
            //   fontWeight: 'normal',
            },
            xAxis: {
              data: <%= session.getAttribute("subject")%>,
              axisLabel: {
                interval:0,
                rotate:40,
              },
            },
            yAxis: {},
            series: [
              {
                type: 'bar',
                data: <%= session.getAttribute("score")%>,
                itemStyle: {
                  normal: {
                    label: {
                      show: true, //开启显示
                      position: 'top', //在上方显示
                      textStyle: { //数值样式
                        color: 'black',
                        fontSize: 16
                      }
                    }
                  }
                }
              }
            ]
          }
        myChart.setOption(option)
      </script>
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="<%= basePath%>resources/js/jquery-3.2.1.min.js"></script>
    <script src="<%= basePath%>resources/js/popper.min.js"></script>
    <script src="<%= basePath%>resources/js/bootstrap.min.js"></script>
    <script src="<%= basePath%>resources/js/main.js"></script>
  </body>
</html>